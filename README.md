# Explaining the photosynthetic Gibbs effect

[![pipeline status](https://gitlab.com/qtb-hhu/modelbase-software/badges/main/pipeline.svg)](https://gitlab.com/qtb-hhu/photosynthesis-task-force/2022-explaining-the-photosynthetic-gibbs-effect/-/commits/main)
[![download pdf](https://img.shields.io/badge/Main-PDF-green)](https://gitlab.com/qtb-hhu/photosynthesis-task-force/2022-explaining-the-photosynthetic-gibbs-effect/-/jobs/artifacts/main/file/paper/main.pdf?job=build)


## Setting up

- `Install Python >= 3.11`
- `pip install -r code/requirements.txt`
- `pre-commit install`
