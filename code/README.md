# Install dependencies

`pip install -r requirements.txt`

## Update dependencies

`pip install pip-tools`
`pip-compile --generate-hashes requirements.in`
`pip-sync`

## Description

To quantitatively study the Gibbs effect with theoretical models, we pursue the following strategy.

1. Implement and adapt a published kinetic model (Pettersson 1988,Poolman 2000) including all carbon converting steps of the CBB cycle, so that under realistic stationary conditions it reproduces experimentally measured intermediate concentration.
2. Transform the model to reflect all possible isotopomers. For this, every intermediate is represented by all possible $2^n$ isotopomer versions, where $n$ denotes the number of carbon atoms.
    1. Split the model into forward an backward reactions
    2. Perform simulations, in which a constant concentration of fully labelled carbon dioxide was assumed
3. In contrast to the previous models, which described fast reversible reactions by a rapid equilibrium, we determine the disequilibrium of the involved reactions explicitly, because a rapid equilibrium lead to an instant equilibration of the labels in the respective intermediates, and thus show no Gibbs effect (see below).
4. We transform the disequilibrium model into the label model
5. We systematically scan the label distribution for changes to the photosynthetic Gibbs effect


## Recreation of the Poolman model

The Pettersson model describes the calvin cycle using 16 dynamic variables (ATP, PGA, BPGA, GAP, DHAP, FBP, F6P, G6P, G1P, SBP, S7P, E4P, X5P, R5P, RU5P, RUBP) and a total of 19 reactions. 

The model distinguishes between a set of reactions assumed to be at equilibrium and a set assumed to be displaced from equilibrium. This is done since it is assumed that only non-equilibrium steps are of primary regulatory interest.

The equilibrium reactions are the following

\begin{equation}
\begin{split} 
	[BPGA][ADP] &= q_2 [PGA][ATP] \\
	[GAP][NADP^+][P_i] &= q_3 [BPGA][NADPH][H^+] \\
	[DHAP] &= q_4 [GAP] \\
	[FBP] &= q_5 [GAP][DHAP] \\
	[X5P][E4P] &= q_7 [GAP][F6P] \\
	[SBP] &= q_8 [DHAP][E4P] \\
	[X5P][R5P] &= q_10 [GAP][S7P] \\
	[Ru5P] &= q_{11} [R5P] \\
	[Ru5P] &= q_{12}[X5P] \\
	[G6P] &= q_{14} [F6P] \\
	[G1P] &= q_15 [G6P]	
\end{split}
\end{equation}

Each of the non-equilibrium reactions contains regulatory terms $R(S)$, giving the canonical expression

\begin{equation}
	v = \frac{V_{Max} [S]}{[S] + K_m \cdot R(S)}
\end{equation}

which together with the assumed regulatory mechanism yields the following expressions:

\begin{equation}
	\begin{split}
	v_1 &= \mathrm{\frac{V_1 [RuBP]}{[RuBP] + K_{m1} \left( 1 + \frac{[PGA]}{K_{i11}} + \frac{[FBP]}{K_{i12}} + \frac{[SBP]}{K_{i13}} + \frac{[P_i]}{K_{i14}} + \frac{[NADPH]}{K_{i15}}\right)}} \\
	v_6 &= \mathrm{\frac{V_6 [FBP]}{[FBP] + K_{m6} \left( 1 + \frac{[F6P]}{K_{i61}} + \frac{[P_i]}{K_{i62}}\right)}} \\
	v_9 &= \mathrm{\frac{V_9 [SBP]}{[SBP] + K_{m9} \left(1 + \frac{[P_i]}{K_{i9}}\right)}} \\
	v_{13} &= \mathrm{\frac{V_{13}[Ru5P][ATP]}{\left[[Ru5P] + K_{m131} \left(1 + \frac{[PGA]}{K_{i131}} + \frac{[RuBP]}{K_{i132}} + \frac{[P_i]}{K_{i133}} \right) \right] \cdot \left[ [ATP] \left( 1 + \frac{[ADP]}{K_{i134}} \right) + K_{m132} \left(1 + \frac{[ADP]}{K_{i135}} \right) \right]}} \\
	v_{16} &= \frac{V_{16} [ADP][P_i]}{([ADP] + K_{m161}) \cdot ([P_i] + K_{m162})}
	\end{split}
\end{equation}

Besides the simplified starch production pathway ($v_{16}$), the model has triose phosphate translocators as further system outputs.


\begin{equation}
    \begin{split}
        v_{PGA} &= \frac{V_{ex} [PGA]}{N \cdot K_{PGA}} \\
        v_{GAP} &= \frac{V_{ex} [GAP]}{N \cdot K_{GAP}} \\
        v_{DHAP} &= \frac{V_{ex} [DHAP]}{N \cdot K_{DHAP}} \\
    \end{split}
\end{equation}

with

\begin{equation}
\begin{split}
	\mathrm{N} &= \mathrm{1 + \left(1 + \frac{K_{P_{ext}}}{[P_{ext}} \right) \cdot \left( \frac{[P_i]}{K_{P_i}} + \frac{[PGA]}{K_{PGA}} + \frac{[GAP]}{K_{GAP}} + \frac{[DHAP]}{K_{DHAP}}\right)} \\
\end{split}
\end{equation}

Lastly, the model contains the following three conserved quantities, which are used to track the ATP and inorganic phosphate concentration and the total amount of exported triose phosphates.  

\begin{equation}
\begin{split}
	\mathrm{AP_{total}} &= \mathrm{ADP + ATP} \\
	\mathrm{P_{total}} &= \mathrm{P_i} + \mathrm{([PGA] + 2 \cdot [BPGA] + [GAP] + [DHAP] + }\\
	&\qquad \qquad \qquad \mathrm{2 \cdot [FBP] + [F6P] + [G6P] + [G1P] + }\\
	&\qquad \qquad \qquad \mathrm{2 \cdot [SBP] + [S7P] + [E4P] + [X5P] + }\\
	&\qquad \qquad \qquad \mathrm{[R5P] + 2 \cdot [RuBP] + [Ru5P] + [ATP])}
\end{split}
\end{equation}

Combined with the stoichiometry of the model this leads to the following system of ODEs:

\begin{align*}
\frac{d\mathrm{PGA}}{\mathrm{dt}} &= 2 \cdot v_1 - v_2 - v_{\mathrm{PGA}} \\
\frac{d\mathrm{BPGA}}{\mathrm{dt}} &= v_2 - v_3 \\
\frac{d\mathrm{GAP}}{\mathrm{dt}} &= v_3 - v_4 - v_5 - v_7 - v_{10} - v_{\mathrm{GAP}} \\
\frac{d\mathrm{DHAP}}{\mathrm{dt}} &= v_4 - v_5 - v_8 - v_{\mathrm{DHAP}} \\
\frac{d\mathrm{FBP}}{\mathrm{dt}} &= v_5 - v_6 \\
\frac{d\mathrm{F6P}}{\mathrm{dt}} &= v_6 - v_7 - v_{14} \\
\frac{d\mathrm{E4P}}{\mathrm{dt}} &= v_7 - v_8 \\
\frac{d\mathrm{SBP}}{\mathrm{dt}} &= v_8 - v_9 \\
\frac{d\mathrm{S7P}}{\mathrm{dt}} &= v_9 - v_{10} \\
\frac{d\mathrm{X5P}}{\mathrm{dt}} &= v_7 + v_{10} - v_{12} \\
\frac{d\mathrm{R5P}}{\mathrm{dt}} &= v_{10} - v_{11} \\
\frac{d\mathrm{Ru5P}}{\mathrm{dt}} &= v_{11} + v_{12} - v_{13} \\
\frac{d\mathrm{RuBP}}{\mathrm{dt}} &= v_{13} - v_1 \\
\frac{d\mathrm{G6P}}{\mathrm{dt}} &= v_{14} - v_{15} \\
\frac{d\mathrm{G1P}}{\mathrm{dt}} &= v_{15} - v_{\mathrm{Starch}} \\
\frac{d\mathrm{ATP}}{\mathrm{dt}} &= v_{16} - v_2 - v_{13} - v_{\mathrm{Starch}} \\
\end{align*}

The Poolman variant uses reversible mass action kinetics to model the reactions previously assumed to be at equilibrium, described by rate function

\begin{equation}
	v = k_{re} \left( \prod_{i = 1}^{n_s} S_i - \frac{\prod_{j = 1}^{n_p}P_j}{K_{eq}} \right)
\end{equation} 

with $k_{re} = 8\cdot 10^8$ regardless of the reaction molecularity 

## Determining disequilibrium

As can be seen above, the rapid equilibrium assumption by Pettersson leads to instant equilibration of the labels in the respective intermediates, and thus shows no Gibbs effect. We thus now determine the disequilibrium of the involved reactions explicitly. This involves in particular the identification of forward and backward rate constants of the reversible reactions (transketolase, aldolase, isomerases, epimerase), which can be uniquely determined from the measured concentrations and the equilibrium constants.

### Disequilibrium for rapid equilibrium reactions

\begin{equation}
\begin{split}
&\phantom{\Leftrightarrow \ } v^+ - v^- = v \\ 
&\Leftrightarrow k^+ \prod_i S_i - k^- \prod_j P_j = v \\ 
&\Leftrightarrow k^+ = \frac{v}{\prod_i S_i - \frac{k^-}{k^+} \prod_j P_j} \\
&\Leftrightarrow k^+ = \frac{\frac{v}{\prod_i S_i}}{1 - \frac{Q}{K_{eq}}}
\end{split}
\end{equation}

likewise

\begin{equation}
\begin{split}
%v^+ - v^- &= v \\ %&&| \; v^+ = k^+ \prod_i S_i, v^- = k^- \prod_j P_j 
%k^+ \prod_i S_i - k^- \prod_j P_j &= v \\ 
%k^- \left(\frac{k^+}{k^-} \prod_i S_i - \prod_j P_j \right) &= v \\
%k^- &= \frac{v}{\frac{k^+}{k^-} \prod_i S_i - \prod_j P_j} \\
%k^- &= \frac{\frac{v}{\prod_j P_j}}{\frac{k^+}{k^-} \frac{\prod_i S_i}{\prod_j P_j} - 1} \\ %&&|\; Q = \frac{\prod_j P_j}{\prod_i S_i}
%k^- &= \frac{\frac{v}{\prod_j P_j}}{\frac{k^+}{k^-} \frac{1}{Q} - 1} \\ %&&| \; K_{eq} = \frac{k^+}{k^-} 
&\phantom{\Leftrightarrow \ } k^- = - \frac{\frac{v}{\prod_j P_j}}{1 - \frac{K_{eq}}{Q}}
\end{split}
\end{equation}

$Q$ and $K_{eq}$ are calculated using the following equations

\begin{equation}
\Delta G = \Delta G^0 + R \cdot T \cdot ln(Q)
\end{equation}

\begin{equation}
\Delta G^0 = - R \cdot T \cdot ln(K_{eq})
\end{equation}

With the new obtained mass action rate parameters the corresponding reactions were brought into standard mass action format

\begin{equation}
	v = k^+ \prod_{i}S_i - k^- \prod_{j} P_j
\end{equation}

### Fitting the regularised reactions

Similiarly, in order to fit the regularised reactions that follow the canonical expression

\begin{equation}
	v = \frac{V_{Max} \cdot [S]}{[S] + K_m \cdot R(S)}
\end{equation}

the $V_{Max}$ values were obtained using equation

\begin{equation}
	V_{Max} = \frac{v \left( [S] + K_m \cdot R(S) \right)}{[S]}
\end{equation}

and the experimentally measured concentrations.
