from __future__ import annotations

from typing import TYPE_CHECKING

from modelbase.ode.utils import ratefunctions as rf

from .poolman_split import get_poolman_model_split

if TYPE_CHECKING:
    from modelbase.ode import Model


def v3r(
    GAP: float,
    P_i: float,
    k3r: float,
    NADP_pool: float,
) -> float:
    return k3r * GAP * NADP_pool * P_i


def get_bassham_model(new_k: dict[str, float], new_vmax: dict[str, float]) -> Model:
    m = get_poolman_model_split()
    m.remove_parameters(
        [
            "q2",
            "q3",
            "q4",
            "q5",
            "q7",
            "q8",
            "q10",
            "q11",
            "q12",
            "q14",
            "q15",
            "Vmax_efflux",
        ]
    )
    m.add_and_update_parameters(new_k)
    m.add_and_update_parameters(new_vmax)

    m.update_reaction(rate_name="v2r", function=rf.mass_action_2, parameters=["k2r"])
    m.update_reaction(rate_name="v3r", function=v3r, parameters=["k3r", "NADP_pool"])
    m.update_reaction(rate_name="v4r", function=rf.mass_action_1, parameters=["k4r"])
    m.update_reaction(
        rate_name="ALD_DHAP_GAP_FBP_bwd", function=rf.mass_action_1, parameters=["k5r"]
    )
    m.update_reaction(
        rate_name="ALD_DHAP_E4P_SBP_bwd", function=rf.mass_action_1, parameters=["k8r"]
    )
    m.update_reaction(
        rate_name="TK_X5P_E4P_F6P_GAP_fwd",
        function=rf.mass_action_2,
        parameters=["k7r"],
    )
    m.update_reaction(
        rate_name="TK_X5P_R5P_S7P_GAP_fwd",
        function=rf.mass_action_2,
        parameters=["k10r"],
    )
    m.update_reaction(rate_name="v11r", function=rf.mass_action_1, parameters=["k11r"])
    m.update_reaction(rate_name="v12r", function=rf.mass_action_1, parameters=["k12r"])
    m.update_reaction(rate_name="v14r", function=rf.mass_action_1, parameters=["k14r"])
    m.update_reaction(rate_name="v15r", function=rf.mass_action_1, parameters=["k15r"])

    m.update_reaction(
        rate_name="vPGA_out",
        parameters=["Vmax_efflux_PGA", "K_pga"],
    )
    m.update_reaction(
        rate_name="vGAP_out",
        parameters=["Vmax_efflux_GAP", "K_gap"],
    )
    m.update_reaction(
        rate_name="vDHAP_out",
        parameters=["Vmax_efflux_DHAP", "K_dhap"],
    )
    return m
