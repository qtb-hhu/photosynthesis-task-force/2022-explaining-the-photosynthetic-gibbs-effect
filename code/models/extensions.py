from __future__ import annotations

from typing import TYPE_CHECKING

from modelbase.ode.models.model import Model
from modelbase.ode.utils import ratefunctions as rf

if TYPE_CHECKING:
    from modelbase.ode import Model


def proportional(x: float, y: float) -> float:
    return x * y


def div(x: float, y: float) -> float:
    return x / y


def get_tk_names(
    *,
    standard: bool = False,
    nonstandard: bool = False,
    neutral: bool = False,
    octulose: bool = False,
) -> list[str]:
    rxns = []
    if standard:
        rxns.extend(
            [
                "TK_X5P_E4P_F6P_GAP_bwd",
                "TK_X5P_E4P_F6P_GAP_fwd",
                "TK_X5P_R5P_S7P_GAP_bwd",
                "TK_X5P_R5P_S7P_GAP_fwd",
            ]
        )
    if nonstandard:
        rxns.extend(
            [
                "TK_F6P_R5P_S7P_E4P_fwd",
                "TK_F6P_R5P_S7P_E4P_bwd",
            ]
        )
    if neutral:
        rxns.extend(
            [
                "TK_X5P_GAP_X5P_GAP",
                "TK_F6P_E4P_F6P_E4P",
                "TK_S7P_R5P_S7P_R5P",
                "TK_O8P_G6P_O8P_G6P",
            ]
        )
    if octulose:
        rxns.extend(
            [
                "TK_X5P_G6P_O8P_GAP_fwd",
                "TK_X5P_G6P_O8P_GAP_bwd",
                "TK_F6P_G6P_O8P_E4P_fwd",
                "TK_F6P_G6P_O8P_E4P_bwd",
                "TK_S7P_G6P_O8P_R5P_fwd",
                "TK_S7P_G6P_O8P_R5P_bwd",
            ]
        )

    return rxns


def get_ald_names(*, standard: bool = False, extended: bool = False) -> list[str]:
    names = []
    if standard:
        names.extend(
            [
                "ALD_DHAP_GAP_FBP_fwd",
                "ALD_DHAP_GAP_FBP_bwd",
                "ALD_DHAP_E4P_SBP_fwd",
                "ALD_DHAP_E4P_SBP_bwd",
            ]
        )
    if extended:
        names.extend(
            [
                "ALD_DHAP_A5P_OBP_fwd",
                "ALD_DHAP_A5P_OBP_bwd",
            ]
        )
    return names


def get_tr_names() -> list[str]:
    return [
        "TRALD_F6P_E4P_S7P_GAP_fwd",
        "TRALD_F6P_E4P_S7P_GAP_bwd",
        "TRALD_F6P_A5P_O8P_GAP_fwd",
        "TRALD_F6P_A5P_O8P_GAP_bwd",
    ]


def get_transketolase_maps() -> dict[str, list[int]]:
    def tk_labelmap(donor: int, acceptor: int) -> list[int]:
        lm = [0, 1]
        lm.extend(range(donor, donor + acceptor))
        lm.extend(range(2, donor))
        return lm

    return {
        # Donor + Acceptor <=> Donor + Acceptor
        # fmt: off
        # neutral reactions
        "TK_X5P_GAP_X5P_GAP": tk_labelmap(5, 3),  # TK_X5P_GAP_X5P_GAP
        "TK_F6P_E4P_F6P_E4P": tk_labelmap(6, 4),  # TK_F6P_E4P_F6P_E4P
        "TK_S7P_R5P_S7P_R5P": tk_labelmap(7, 5),  # TK_S7P_R5P_S7P_R5P
        "TK_O8P_G6P_O8P_G6P": tk_labelmap(8, 6),
        # forward reactions
        "TK_X5P_E4P_F6P_GAP_fwd": tk_labelmap(5, 4),  # TK_X5P_E4P_F6P_GAP
        "TK_X5P_R5P_S7P_GAP_fwd": tk_labelmap(5, 5),  # TK_X5P_R5P_S7P_GAP
        "TK_X5P_G6P_O8P_GAP_fwd": tk_labelmap(5, 6),  # TK_X5P_G6P_O8P_GAP
        "TK_F6P_R5P_S7P_E4P_fwd": tk_labelmap(6, 5),  # TK_F6P_R5P_S7P_E4P
        "TK_F6P_G6P_O8P_E4P_fwd": tk_labelmap(6, 6),  # TK_F6P_G6P_O8P_E4P
        "TK_S7P_G6P_O8P_R5P_fwd": tk_labelmap(7, 6),  # TK_S7P_G6P_O8P_R5P
        # backward reactions
        "TK_X5P_E4P_F6P_GAP_bwd": tk_labelmap(6, 3),  # TK_F6P_GAP_X5P_E4P
        "TK_X5P_R5P_S7P_GAP_bwd": tk_labelmap(7, 3),  # TK_S7P_GAP_X5P_R5P
        "TK_F6P_R5P_S7P_E4P_bwd": tk_labelmap(7, 4),  # TK_S7P_E4P_F6P_R5P
        "TK_X5P_G6P_O8P_GAP_bwd": tk_labelmap(8, 3),  # TK_O8P_GAP_X5P_G6P
        "TK_F6P_G6P_O8P_E4P_bwd": tk_labelmap(8, 4),  # TK_O8P_E4P_F6P_G6P
        "TK_S7P_G6P_O8P_R5P_bwd": tk_labelmap(8, 5),  # TK_O8P_R5P_S7P_G6P
        # fmt: on
    }


def get_aldolase_maps() -> dict[str, list[int]]:
    return {
        "ALD_DHAP_GAP_FBP_fwd": [0, 1, 2, 3, 4, 5],
        "ALD_DHAP_GAP_FBP_bwd": [0, 1, 2, 3, 4, 5],
        "ALD_DHAP_E4P_SBP_fwd": [0, 1, 2, 3, 4, 5, 6],
        "ALD_DHAP_E4P_SBP_bwd": [0, 1, 2, 3, 4, 5, 6],
        "ALD_DHAP_A5P_OBP_fwd": [0, 1, 2, 3, 4, 5, 6, 7],
        "ALD_DHAP_A5P_OBP_bwd": [0, 1, 2, 3, 4, 5, 6, 7],
    }


def get_transaldolase_maps() -> dict[str, list[int]]:
    def transaldolase_labelmap(donor: int, acceptor: int) -> list[int]:
        lm = [0, 1, 2]
        lm.extend(range(donor, donor + acceptor))
        lm.extend(range(3, donor))
        return lm

    # fmt: off
    return {
        # Donor + Acceptor <=> Donor + Acceptor
        "TRALD_F6P_E4P_S7P_GAP_fwd": transaldolase_labelmap(6, 4),
        "TRALD_F6P_E4P_S7P_GAP_bwd": transaldolase_labelmap(7, 3),  # TRALD_S7P_GAP_F6P_E4P
        "TRALD_F6P_A5P_O8P_GAP_fwd": transaldolase_labelmap(6, 5),
        "TRALD_F6P_A5P_O8P_GAP_bwd": transaldolase_labelmap(8, 3),  # TRALD_O8P_GAP_F6P_A5P
    }
    # fmt: on


def get_label_compounds() -> dict[str, int]:
    return {
        "PGA": 3,
        "BPGA": 3,
        "GAP": 3,
        "DHAP": 3,
        "FBP": 6,
        "F6P": 6,
        "E4P": 4,
        "G6P": 6,
        "G1P": 6,
        "SBP": 7,
        "S7P": 7,
        "X5P": 5,
        "R5P": 5,
        "RU5P": 5,
        "RUBP": 5,
        # Extended
        "A5P": 5,  # Arabinose-5-phosphate
        "O8P": 8,  # Octulose 8-phosphate
        "OBP": 8,  # Octulose 1,8-bisphosphate
    }


def get_label_maps() -> dict[str, list[int]]:
    return (
        {
            "v1": [2, 1, 0, 5, 3, 4],
            "v2f": [0, 1, 2],
            "v2r": [0, 1, 2],
            "v3f": [0, 1, 2],
            "v3r": [0, 1, 2],
            "v4f": [2, 1, 0],
            "v4r": [2, 1, 0],
            "v6": [0, 1, 2, 3, 4, 5],
            "v9": [0, 1, 2, 3, 4, 5, 6],
            "v11f": [0, 1, 2, 3, 4],
            "v11r": [0, 1, 2, 3, 4],
            "v12f": [0, 1, 2, 3, 4],
            "v12r": [0, 1, 2, 3, 4],
            "v13": [0, 1, 2, 3, 4],
            "v14f": [0, 1, 2, 3, 4, 5],
            "v14r": [0, 1, 2, 3, 4, 5],
            "v15f": [0, 1, 2, 3, 4, 5],
            "v15r": [0, 1, 2, 3, 4, 5],
            "vSt": [0, 1, 2, 3, 4, 5],
            "vPGA_out": [0, 1, 2],
            "vGAP_out": [0, 1, 2],
            "vDHAP_out": [0, 1, 2],
        }
        | get_transketolase_maps()
        | get_aldolase_maps()
        | get_transaldolase_maps()
        | {
            "a5p_iso_fwd": [0, 1, 2, 3, 4],
            "a5p_iso_bwd": [0, 1, 2, 3, 4],
            "OBPase": [0, 1, 2, 3, 4, 5, 6, 7],
        }
    )


def add_transketolase_extended(
    m: Model,
    *,
    non_standard: bool = True,
    neutral: bool = True,
    octulose: bool = True,
    scale: float = 0.1,
) -> Model:
    if octulose and "O8P" not in m.compounds:
        m.add_compound("O8P")

    # Derive from transketolase parameter
    m.add_parameters(
        {
            "tk_scale": scale,
            "keq_TK_F6P_R5P_S7P_E4P": 0.42613530360981794,
            "keq_TK_X5P_G6P_O8P_GAP": 0.13669671964983157,
            "keq_TK_F6P_G6P_O8P_E4P": 0.012399730111558033,
            "keq_TK_S7P_G6P_O8P_R5P": 0.029098105710842186,
        }
    )

    m.add_derived_parameter("kTKf", proportional, ["k7f", "tk_scale"])
    m.add_derived_parameter(
        "k_TK_F6P_R5P_S7P_E4P_r", div, ["kTKf", "keq_TK_F6P_R5P_S7P_E4P"]
    )
    m.add_derived_parameter(
        "k_TK_X5P_G6P_O8P_GAP_r", div, ["kTKf", "keq_TK_X5P_G6P_O8P_GAP"]
    )
    m.add_derived_parameter(
        "k_TK_F6P_G6P_O8P_E4P_r", div, ["kTKf", "keq_TK_F6P_G6P_O8P_E4P"]
    )
    m.add_derived_parameter(
        "k_TK_S7P_G6P_O8P_R5P_r", div, ["kTKf", "keq_TK_S7P_G6P_O8P_R5P"]
    )

    # fmt: off
    if non_standard:
        m.add_reaction("TK_F6P_R5P_S7P_E4P_fwd", rf.mass_action_2, {"F6P": -1, "R5P": -1, "S7P": 1, "E4P": 1}, parameters=["kTKf"])
        m.add_reaction("TK_F6P_R5P_S7P_E4P_bwd", rf.mass_action_2, {"S7P": -1, "E4P": -1, "F6P": 1, "R5P": 1}, parameters=["k_TK_F6P_R5P_S7P_E4P_r"])
    if octulose:
        m.add_reaction("TK_X5P_G6P_O8P_GAP_fwd", rf.mass_action_2, {"X5P": -1, "G6P": -1, "O8P": 1, "GAP": 1}, parameters=["kTKf"])
        m.add_reaction("TK_F6P_G6P_O8P_E4P_fwd", rf.mass_action_2, {"F6P": -1, "G6P": -1, "O8P": 1, "E4P": 1}, parameters=["kTKf"])
        m.add_reaction("TK_S7P_G6P_O8P_R5P_fwd", rf.mass_action_2, {"S7P": -1, "G6P": -1, "O8P": 1, "R5P": 1}, parameters=["kTKf"])
        m.add_reaction("TK_X5P_G6P_O8P_GAP_bwd", rf.mass_action_2, {"O8P": -1, "GAP": -1, "X5P": 1, "G6P": 1}, parameters=["k_TK_X5P_G6P_O8P_GAP_r"])
        m.add_reaction("TK_F6P_G6P_O8P_E4P_bwd", rf.mass_action_2, {"O8P": -1, "E4P": -1, "F6P": 1, "G6P": 1}, parameters=["k_TK_F6P_G6P_O8P_E4P_r"])
        m.add_reaction("TK_S7P_G6P_O8P_R5P_bwd", rf.mass_action_2, {"O8P": -1, "R5P": -1, "S7P": 1, "G6P": 1}, parameters=["k_TK_S7P_G6P_O8P_R5P_r"])
    if neutral:
        m.add_reaction_from_args("TK_X5P_GAP_X5P_GAP", rf.mass_action_2, {}, args=["X5P", "GAP", "kTKf"])
        m.add_reaction_from_args("TK_F6P_E4P_F6P_E4P", rf.mass_action_2, {}, args=["F6P", "E4P", "kTKf"])
        m.add_reaction_from_args("TK_S7P_R5P_S7P_R5P", rf.mass_action_2, {}, args=["S7P", "R5P", "kTKf"])
    if neutral and octulose:
        m.add_reaction_from_args("TK_O8P_G6P_O8P_G6P", rf.mass_action_2, {}, args=["O8P", "G6P", "kTKf"])
    # fmt: on
    return m


def add_aldolase_extended(m: Model, scale: float = 0.1) -> Model:
    for cpd in ["A5P", "OBP"]:
        if cpd not in m.compounds:
            m.add_compound(cpd)

    # Derive from aldolase parameter
    m.add_parameters(
        {
            "ald_scale": scale,
            "keq_ALD_DHAP_A5P_OBP": 0.050971775847119774,
        }
    )
    m.add_derived_parameter("kALDf", proportional, ["k5f", "ald_scale"])
    m.add_derived_parameter("kALDr", div, ["k5f", "keq_ALD_DHAP_A5P_OBP"])

    # fmt: off
    m.add_reaction("ALD_DHAP_A5P_OBP_fwd", rf.mass_action_2, {"DHAP": -1, "A5P": -1, "OBP": 1}, parameters=["kALDf"])
    m.add_reaction("ALD_DHAP_A5P_OBP_bwd", rf.mass_action_1, {"OBP": -1, "DHAP": 1, "A5P": 1}, parameters=["kALDr"])
    # fmt: on
    return m


def add_transaldolase(m: Model, scale: float = 0.1) -> Model:
    """
    Equilibrator:
        Fructose-6-phosphate(aq) + Erythrose-4-phosphate(aq) ⇌ Sedoheptulose-7-phosphate(aq) + Glyceraldehyde 3-phosphate(aq)
        K'eq = 0.7
    """
    for cpd in ["A5P", "O8P"]:
        if cpd not in m.compounds:
            m.add_compound(cpd)

    # For lack of better parameters also make this dependent on aldolase
    m.add_parameters(
        {
            "trald_scale": scale,
            "keq_TRALD_F6P_E4P_S7P_GAP": 3.974128194669281,
            "keq_TRALD_F6P_A5P_O8P_GAP": 0.12588810693203276,
        }
    )
    m.add_derived_parameter("kTRf", proportional, ["k5f", "trald_scale"])
    m.add_derived_parameter(
        "k_TRALD_F6P_E4P_S7P_GAP_r", div, ["kTRf", "keq_TRALD_F6P_E4P_S7P_GAP"]
    )
    m.add_derived_parameter(
        "k_TRALD_F6P_A5P_O8P_GAP_r", div, ["kTRf", "keq_TRALD_F6P_A5P_O8P_GAP"]
    )

    # fmt: off
    m.add_reaction("TRALD_F6P_E4P_S7P_GAP_fwd", rf.mass_action_2,
                  {"F6P": -1, "E4P": -1, "S7P": 1, "GAP": 1},
                  parameters=["kTRf"])
    m.add_reaction("TRALD_F6P_A5P_O8P_GAP_fwd", rf.mass_action_2,
                  {"F6P": -1, "A5P": -1, "O8P": 1, "GAP": 1},
                  parameters=["kTRf"])
    m.add_reaction("TRALD_F6P_E4P_S7P_GAP_bwd", rf.mass_action_2,
                  {"S7P": -1, "GAP": -1, "F6P": 1, "E4P": 1},
                  parameters=["k_TRALD_F6P_E4P_S7P_GAP_r"])
    m.add_reaction("TRALD_F6P_A5P_O8P_GAP_bwd", rf.mass_action_2,
                  {"O8P": -1, "GAP": -1, "F6P": 1, "A5P": 1},
                  parameters=["k_TRALD_F6P_A5P_O8P_GAP_r"])
    # fmt: on
    return m


def add_a5p_isopmerase(m: Model, scale: float = 0.1) -> Model:
    # Basing kinetics on Ribose-5-phosphat-Isomerase (v11)
    for cpd in ["A5P"]:
        if cpd not in m.compounds:
            m.add_compound(cpd)

    m.add_parameters(
        {
            "a5p_scale": scale,
            "keq_a5p_isomerase": 0.37325254049763423,
        }
    )
    m.add_derived_parameter("kA5PIf", proportional, ["k11f", "a5p_scale"])
    m.add_derived_parameter("kA5PIr", div, ["kA5PIf", "keq_a5p_isomerase"])

    # fmt: off
    m.add_reaction("a5p_iso_fwd", rf.mass_action_1, {"A5P": -1, "RU5P": 1}, parameters=["kA5PIf"])
    m.add_reaction("a5p_iso_bwd", rf.mass_action_1, {"RU5P": -1, "A5P": 1}, parameters=["kA5PIr"])
    # fmt: on
    return m


def add_obpase(m: Model, scale: float = 0.1) -> Model:
    from models.poolman import v9

    for cpd in ["O8P", "OBP"]:
        if cpd not in m.compounds:
            m.add_compound(cpd)

    # Basing kinetics on SBPase (v9)
    m.add_parameter("obpase_scale", scale)
    m.add_derived_parameter("Vmax_obpase", proportional, ["Vmax_9", "obpase_scale"])

    m.add_reaction_from_args(
        "OBPase",
        v9,
        stoichiometry={"OBP": -1, "O8P": 1},
        args=["OBP", "P_pool", "Vmax_obpase", "Km_9", "Ki_9"],
    )
    return m


def get_model_variant(
    m: Model,
    *,
    tk: bool = False,
    ald: bool = False,
    a5p: bool = False,
    tr: bool = False,
    obpase: bool = False,
    tk_include_non_standard: bool = True,
    tk_include_neutral: bool = True,
    tk_include_octulose: bool = True,
    scale: float = 0.1,
) -> Model:
    if tk:
        m = add_transketolase_extended(
            m,
            scale=scale,
            non_standard=tk_include_non_standard,
            neutral=tk_include_neutral,
            octulose=tk_include_octulose,
        )
    if ald:
        m = add_aldolase_extended(m, scale=scale)
    if a5p:
        m = add_a5p_isopmerase(m, scale=scale)
    if tr:
        m = add_transaldolase(m, scale=scale)
    if obpase:
        m = add_obpase(m, scale=scale)
    return m
