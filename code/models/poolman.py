from __future__ import annotations

from modelbase.ode import Model

###############################################################################
# Algebraic modules
###############################################################################


def ADP(ATP: float, AP_total: float) -> list[float]:
    return [AP_total - ATP]


def P_i(
    PGA: float,
    BPGA: float,
    GAP: float,
    DHAP: float,
    FBP: float,
    F6P: float,
    G6P: float,
    G1P: float,
    SBP: float,
    S7P: float,
    E4P: float,
    X5P: float,
    R5P: float,
    RUBP: float,
    RU5P: float,
    ATP: float,
    phosphate_total: float,
) -> list[float]:
    return [
        phosphate_total
        - (
            PGA
            + 2 * BPGA
            + GAP
            + DHAP
            + 2 * FBP
            + F6P
            + G6P
            + G1P
            + 2 * SBP
            + S7P
            + E4P
            + X5P
            + R5P
            + 2 * RUBP
            + RU5P
            + ATP
        )
    ]


def N(
    P_pool: float,
    PGA: float,
    GAP: float,
    DHAP: float,
    Kpxt: float,
    Pext: float,
    Kpi: float,
    Kpga: float,
    Kgap: float,
    Kdhap: float,
) -> list[float]:
    return [
        (
            1
            + (1 + (Kpxt / Pext))
            * ((P_pool / Kpi) + (PGA / Kpga) + (GAP / Kgap) + (DHAP / Kdhap))
        )
    ]


###############################################################################
# Equilibrium functions
###############################################################################


def rapid_equilibrium_1_1(
    s1: float,
    p1: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 - p1 / q)


def rapid_equilibrium_2_1(
    s1: float,
    s2: float,
    p1: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 * s2 - p1 / q)


def rapid_equilibrium_2_2(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 * s2 - (p1 * p2) / q)


def v3(
    BPGA: float,
    GAP: float,
    phosphate_pool: float,
    proton_pool_stroma: float,
    NADPH_pool: float,
    NADP_pool: float,
    kRE: float,
    q3: float,
) -> float:
    return kRE * (
        (NADPH_pool * BPGA * proton_pool_stroma)
        - (1 / q3) * (GAP * NADP_pool * phosphate_pool)
    )


###############################################################################
# Michaelis-Menten
###############################################################################


def v1(
    RUBP: float,
    PGA: float,
    FBP: float,
    SBP: float,
    P_pool: float,
    V1: float,
    Km1: float,
    Ki11: float,
    Ki12: float,
    Ki13: float,
    Ki14: float,
    Ki15: float,
    NADPH_pool: float,
) -> float:
    return (V1 * RUBP) / (
        RUBP
        + Km1
        * (
            1
            + (PGA / Ki11)
            + (FBP / Ki12)
            + (SBP / Ki13)
            + (P_pool / Ki14)
            + (NADPH_pool / Ki15)
        )
    )


def v6(
    FBP: float,
    F6P: float,
    P: float,
    V6: float,
    Km6: float,
    Ki61: float,
    Ki62: float,
) -> float:
    return (V6 * FBP) / (FBP + Km6 * (1 + (F6P / Ki61) + (P / Ki62)))


def v9(
    SBP: float,
    P: float,
    V9: float,
    Km9: float,
    Ki9: float,
) -> float:
    return (V9 * SBP) / (SBP + Km9 * (1 + (P / Ki9)))


def v13(
    RU5P: float,
    ATP: float,
    PGA: float,
    RUBP: float,
    ADP: float,
    P_pool: float,
    V13: float,
    Km131: float,
    Km132: float,
    Ki131: float,
    Ki132: float,
    Ki133: float,
    Ki134: float,
    Ki135: float,
) -> float:
    return (V13 * RU5P * ATP) / (
        (RU5P + Km131 * (1 + (PGA / Ki131) + (RUBP / Ki132) + (P_pool / Ki133)))
        * (ATP * (1 + (ADP / Ki134)) + Km132 * (1 + (ADP / Ki135)))
    )


def v16(
    ADP: float,
    Phosphate_i: float,
    V16: float,
    Km161: float,
    Km162: float,
) -> float:
    return (V16 * ADP * Phosphate_i) / ((ADP + Km161) * (Phosphate_i + Km162))


def vStarchProduction(
    G1P: float,
    ATP: float,
    PGA: float,
    F6P: float,
    FBP: float,
    ADP: float,
    P_pool: float,
    Vst: float,
    Kmst1: float,
    Kmst2: float,
    Kist: float,
    Kast1: float,
    Kast2: float,
    Kast3: float,
) -> float:
    return (Vst * G1P * ATP) / (
        (G1P + Kmst1)
        * (
            (1 + (ADP / Kist)) * (ATP + Kmst2)
            + ((Kmst2 * P_pool) / (Kast1 * PGA + Kast2 * F6P + Kast3 * FBP))
        )
    )


def v_out(
    s1: float,
    N_total: float,
    VMax_efflux: float,
    K_efflux: float,
) -> float:
    return (VMax_efflux * s1) / (N_total * K_efflux)


def get_poolman_model() -> Model:
    parameters_poolman = {
        "Vmax_1": 2.72,  # [mM/s], Pettersson 1988
        "Vmax_6": 1.6,  # [mM/s], Pettersson 1988
        "Vmax_9": 0.32,  # [mM/s], Pettersson 1988
        "Vmax_13": 8.0,  # [mM/s], Pettersson 1988
        "Vmax_16": 2.8,  # [mM/s], Pettersson 1988
        "Vmax_starch": 0.32,  # [mM/s], Pettersson 1988
        "Vmax_efflux": 2.0,  # [mM/s], Pettersson 1988
        "Km_1": 0.02,  # [mM], Pettersson 1988
        "Km_6": 0.03,  # [mM], Pettersson 1988
        "Km_9": 0.013,  # [mM], Pettersson 1988
        "Km_13_1": 0.05,  # [mM], Pettersson 1988
        "Km_13_2": 0.05,  # [mM], Pettersson 1988
        "Km_16_1": 0.014,  # [mM], Pettersson 1988
        "Km_16_2": 0.3,  # [mM], Pettersson 1988
        "Km_starch_1": 0.08,  # [mM], Pettersson 1988
        "Km_starch_2": 0.08,  # [mM], Pettersson 1988
        "K_pga": 0.25,  # [mM], Pettersson 1988
        "K_gap": 0.075,  # [mM], Pettersson 1988
        "K_dhap": 0.077,  # [mM], Pettersson 1988
        "K_pi": 0.63,  # [mM], Pettersson 1988
        "K_pxt": 0.74,  # [mM], Pettersson 1988
        "Ki_1_1": 0.04,  # [mM], Pettersson 1988
        "Ki_1_2": 0.04,  # [mM], Pettersson 1988
        "Ki_1_3": 0.075,  # [mM], Pettersson 1988
        "Ki_1_4": 0.9,  # [mM], Pettersson 1988
        "Ki_1_5": 0.07,  # [mM], Pettersson 1988
        "Ki_6_1": 0.7,  # [mM], Pettersson 1988
        "Ki_6_2": 12.0,  # [mM], Pettersson 1988
        "Ki_9": 12.0,  # [mM], Pettersson 1988
        "Ki_13_1": 2.0,  # [mM], Pettersson 1988
        "Ki_13_2": 0.7,  # [mM], Pettersson 1988
        "Ki_13_3": 4.0,  # [mM], Pettersson 1988
        "Ki_13_4": 2.5,  # [mM], Pettersson 1988
        "Ki_13_5": 0.4,  # [mM], Pettersson 1988
        "Ki_starch": 10.0,  # [mM], Pettersson 1988
        "Ka_starch_1": 0.1,  # [mM], Pettersson 1988
        "Ka_starch_2": 0.02,  # [mM], Pettersson 1988
        "Ka_starch_3": 0.02,  # [mM], Pettersson 1988
        "q2": 0.00031,  # [], Pettersson 1988
        "q3": 16000000.0,  # [], Pettersson 1988
        "q4": 22.0,  # [], Pettersson 1988
        "q5": 7.1,  # [1/mM]], Pettersson 1988
        "q7": 0.084,  # [], Pettersson 1988
        "q8": 13.0,  # [1/mM]], Pettersson 1988
        "q10": 0.85,  # [], Pettersson 1988
        "q11": 0.4,  # [], Pettersson 1988
        "q12": 0.67,  # [], Pettersson 1988
        "q14": 2.3,  # [], Pettersson 1988
        "q15": 0.058,  # [], Pettersson 1988
        "CO2": 0.2,  # [mM], Pettersson 1988
        "Phosphate_total": 15.0,  # [mM], Pettersson 1988
        "P_pool_ext": 0.5,  # [mM], Pettersson 1988
        "AP_total": 0.5,  # [mM], Pettersson 1988
        "N_total": 0.5,  # [mM], Pettersson 1988
        "pH_medium": 7.6,  # [], Pettersson 1988
        "pH_stroma": 7.9,  # [], Pettersson 1988
        "proton_pool_stroma": 1.2589254117941661e-05,  # [mM], Pettersson 1988
        "NADPH_pool": 0.21,  # [mM], Pettersson 1988
        "NADP_pool": 0.29,  # [mM], Pettersson 1988
        "k_rapid_eq": 800000000.0,  # Rapid Equilibrium speed
    }

    m = Model()
    m.add_parameters(parameters=parameters_poolman)
    m.add_compounds(
        [
            "ATP",
            "PGA",
            "BPGA",
            "GAP",
            "DHAP",
            "FBP",
            "F6P",
            "E4P",
            "G6P",
            "G1P",
            "SBP",
            "S7P",
            "X5P",
            "R5P",
            "RU5P",
            "RUBP",
        ]
    )

    m.add_algebraic_module(
        module_name="ADP_mod",
        function=ADP,
        compounds=["ATP"],
        derived_compounds=["ADP"],
        parameters=["AP_total"],
    )
    m.add_algebraic_module(
        module_name="Pi_mod",
        function=P_i,
        compounds=[
            "PGA",
            "BPGA",
            "GAP",
            "DHAP",
            "FBP",
            "F6P",
            "G6P",
            "G1P",
            "SBP",
            "S7P",
            "E4P",
            "X5P",
            "R5P",
            "RUBP",
            "RU5P",
            "ATP",
        ],
        derived_compounds=["P_pool"],
        parameters=["Phosphate_total"],
    )
    m.add_algebraic_module(
        module_name="N_mod",
        function=N,
        compounds=[
            "P_pool",
            "PGA",
            "GAP",
            "DHAP",
        ],
        derived_compounds=["N_pool"],
        parameters=[
            "K_pxt",
            "P_pool_ext",
            "K_pi",
            "K_pga",
            "K_gap",
            "K_dhap",
        ],
    )

    m.add_reaction(
        rate_name="v2",
        function=rapid_equilibrium_2_2,
        stoichiometry={"PGA": -1, "ATP": -1, "BPGA": 1},
        modifiers=["ADP"],
        parameters=["k_rapid_eq", "q2"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v3",
        function=v3,
        stoichiometry={"BPGA": -1, "GAP": 1},
        modifiers=["P_pool"],
        parameters=[
            "proton_pool_stroma",
            "NADPH_pool",
            "NADP_pool",
            "k_rapid_eq",
            "q3",
        ],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v4",
        function=rapid_equilibrium_1_1,
        stoichiometry={"GAP": -1, "DHAP": 1},
        parameters=["k_rapid_eq", "q4"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v5",
        function=rapid_equilibrium_2_1,
        stoichiometry={"GAP": -1, "DHAP": -1, "FBP": 1},
        parameters=["k_rapid_eq", "q5"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v7",
        function=rapid_equilibrium_2_2,
        stoichiometry={"GAP": -1, "F6P": -1, "E4P": 1, "X5P": 1},
        parameters=["k_rapid_eq", "q7"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v8",
        function=rapid_equilibrium_2_1,
        stoichiometry={"DHAP": -1, "E4P": -1, "SBP": 1},
        parameters=["k_rapid_eq", "q8"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v10",
        function=rapid_equilibrium_2_2,
        stoichiometry={"GAP": -1, "S7P": -1, "X5P": 1, "R5P": 1},
        parameters=["k_rapid_eq", "q10"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v11",
        function=rapid_equilibrium_1_1,
        stoichiometry={"R5P": -1, "RU5P": 1},
        parameters=["k_rapid_eq", "q11"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v12",
        function=rapid_equilibrium_1_1,
        stoichiometry={"X5P": -1, "RU5P": 1},
        parameters=["k_rapid_eq", "q12"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v14",
        function=rapid_equilibrium_1_1,
        stoichiometry={"F6P": -1, "G6P": 1},
        parameters=["k_rapid_eq", "q14"],
        reversible=True,
    )
    m.add_reaction(
        rate_name="v15",
        function=rapid_equilibrium_1_1,
        stoichiometry={"G6P": -1, "G1P": 1},
        parameters=["k_rapid_eq", "q15"],
        reversible=True,
    )

    m.add_reaction(
        rate_name="v1",
        function=v1,
        stoichiometry={"RUBP": -1, "PGA": 2},
        modifiers=[
            "PGA",
            "FBP",
            "SBP",
            "P_pool",
        ],
        parameters=[
            "Vmax_1",
            "Km_1",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "NADPH_pool",
        ],
    )

    m.add_reaction(
        rate_name="v6",
        function=v6,
        stoichiometry={"FBP": -1, "F6P": 1},
        modifiers=["F6P", "P_pool"],
        parameters=["Vmax_6", "Km_6", "Ki_6_1", "Ki_6_2"],
    )

    m.add_reaction(
        rate_name="v9",
        function=v9,
        stoichiometry={"SBP": -1, "S7P": 1},
        modifiers=["P_pool"],
        parameters=["Vmax_9", "Km_9", "Ki_9"],
    )

    m.add_reaction(
        rate_name="v13",
        function=v13,
        stoichiometry={"RU5P": -1, "ATP": -1, "RUBP": 1},
        modifiers=[
            "PGA",
            "RUBP",
            "ADP",
            "P_pool",
        ],
        parameters=[
            "Vmax_13",
            "Km_13_1",
            "Km_13_2",
            "Ki_13_1",
            "Ki_13_2",
            "Ki_13_3",
            "Ki_13_4",
            "Ki_13_5",
        ],
    )

    m.add_reaction(
        rate_name="v16",
        function=v16,
        stoichiometry={"ATP": 1},
        modifiers=["ADP", "P_pool"],
        parameters=["Vmax_16", "Km_16_1", "Km_16_2"],
    )

    m.add_reaction(
        rate_name="vSt",
        function=vStarchProduction,
        stoichiometry={"G1P": -1, "ATP": -1},
        modifiers=["PGA", "F6P", "FBP", "ADP", "P_pool"],
        parameters=[
            "Vmax_starch",
            "Km_starch_1",
            "Km_starch_2",
            "Ki_starch",
            "Ka_starch_1",
            "Ka_starch_2",
            "Ka_starch_3",
        ],
    )

    m.add_reaction(
        rate_name="vPGA_out",
        function=v_out,
        stoichiometry={"PGA": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux", "K_pga"],
    )
    m.add_reaction(
        rate_name="vGAP_out",
        function=v_out,
        stoichiometry={"GAP": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux", "K_gap"],
    )
    m.add_reaction(
        rate_name="vDHAP_out",
        function=v_out,
        stoichiometry={"DHAP": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux", "K_dhap"],
    )
    return m
