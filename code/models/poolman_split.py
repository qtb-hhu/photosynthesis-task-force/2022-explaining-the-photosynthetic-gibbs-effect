from __future__ import annotations

import numpy as np
from modelbase.ode import Model
from modelbase.ode import ratefunctions as rf

from .poolman import get_poolman_model


def dG0s() -> dict[str, float]:
    return {
        "v2f": 18.8,
        "v3f": -2.3,
        "v4f": -1.8,
        "ALD_DHAP_GAP_FBP_fwd": -5.2,
        "ALD_DHAP_E4P_SBP_fwd": -5.6,
        "TK_X5P_E4P_F6P_GAP_bwd": 1.5,
        "TK_X5P_R5P_S7P_GAP_bwd": 0.1,
        "v11f": 0.5,
        "v12f": 0.2,
        "v14f": -0.5,
        "v15f": -8.4,
    }


def dGs() -> dict[str, float]:
    return {
        "v2f": -0.2,
        "v3f": -7.6,
        "v4f": -0.2,
        "ALD_DHAP_GAP_FBP_fwd": -0.4,
        "ALD_DHAP_E4P_SBP_fwd": -0.2,
        "TK_X5P_E4P_F6P_GAP_bwd": -0.9,
        "TK_X5P_R5P_S7P_GAP_bwd": -1.4,
        "v11f": -0.1,
        "v12f": -0.1,
        "v14f": -0.3,
        "v15f": -11.4,
    }


def _new_vmax_v1(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["v1"]
        * (
            y_ss["RUBP"]
            + p["Km_1"]
            * (
                1
                + (y_ss["PGA"] / p["Ki_1_1"])
                + (y_ss["FBP"] / p["Ki_1_2"])
                + (y_ss["SBP"] / p["Ki_1_3"])
                + (y_ss["P_pool"] / p["Ki_1_4"])
                + (p["NADPH_pool"] / p["Ki_1_5"])
            )
        )
        / y_ss["RUBP"]
    )


def _new_vmax_v6(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["v6"]
        * (
            y_ss["FBP"]
            + p["Km_6"]
            * (1 + (y_ss["F6P"] / p["Ki_6_1"]) + (y_ss["P_pool"] / p["Ki_6_2"]))
        )
        / (y_ss["FBP"])
    )


def _new_vmax_v9(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["v9"]
        * (y_ss["SBP"] + p["Km_9"] * (1 + (y_ss["P_pool"] / p["Ki_9"])))
        / (y_ss["SBP"])
    )


def _new_vmax_v13(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["v13"]
        * (
            (
                y_ss["RU5P"]
                + p["Km_13_1"]
                * (
                    1
                    + (y_ss["PGA"] / p["Ki_13_1"])
                    + (y_ss["RUBP"] / p["Ki_13_2"])
                    + (y_ss["P_pool"] / p["Ki_13_3"])
                )
            )
            * (
                y_ss["ATP"] * (1 + (y_ss["ADP"] / p["Ki_13_4"]))
                + p["Km_13_2"] * (1 + (y_ss["ADP"] / p["Ki_13_5"]))
            )
        )
        / (y_ss["RU5P"] * y_ss["ATP"])
    )


def _new_vmax_v16(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["v16"]
        * ((y_ss["ADP"] + p["Km_16_1"]) * (y_ss["P_pool"] + p["Km_16_2"]))
        / (y_ss["ADP"] * y_ss["P_pool"])
    )


def _new_vmax_vStarch(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return (
        v_ss["vSt"]
        * (
            (y_ss["G1P"] + p["Km_starch_1"])
            * (
                (1 + (y_ss["ADP"] / p["Ki_starch"])) * (y_ss["ATP"] + p["Km_starch_2"])
                + (
                    (p["Km_starch_2"] * y_ss["P_pool"])
                    / (
                        p["Ka_starch_1"] * y_ss["PGA"]
                        + p["Ka_starch_2"] * y_ss["F6P"]
                        + p["Ka_starch_3"] * y_ss["FBP"]
                    )
                )
            )
        )
        / (y_ss["G1P"] * y_ss["ATP"])
    )


def _new_vmax_vEfflux_PGA(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return v_ss["vPGA_out"] * (y_ss["N_pool"] * p["K_pga"]) / (y_ss["PGA"])


def _new_vmax_vEfflux_GAP(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return v_ss["vGAP_out"] * (y_ss["N_pool"] * p["K_gap"]) / (y_ss["GAP"])


def _new_vmax_vEfflux_DHAP(
    p: dict[str, float], y_ss: dict[str, float], v_ss: dict[str, float]
) -> float:
    return v_ss["vDHAP_out"] * (y_ss["N_pool"] * p["K_dhap"]) / (y_ss["DHAP"])


def calculate_new_vmax(
    m: Model, y_ss: dict[str, float], v_ss: dict[str, float]
) -> dict[str, float]:
    p = m.parameters
    y_ss = {k: float(v) for k, v in m.get_full_concentration_dict(y=y_ss).items()}
    return {
        "Vmax_1": _new_vmax_v1(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_6": _new_vmax_v6(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_9": _new_vmax_v9(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_13": _new_vmax_v13(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_16": _new_vmax_v16(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_starch": _new_vmax_vStarch(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_efflux_PGA": _new_vmax_vEfflux_PGA(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_efflux_GAP": _new_vmax_vEfflux_GAP(p=p, y_ss=y_ss, v_ss=v_ss),
        "Vmax_efflux_DHAP": _new_vmax_vEfflux_DHAP(p=p, y_ss=y_ss, v_ss=v_ss),
    }


def calculate_disequilibrium(
    m: Model,
    y_ss: dict[str, float],
    v_ss: dict[str, float],
    dG0s: dict[str, float],
    dGs: dict[str, float],
) -> dict[str, float]:
    y_ss = {k: float(v) for k, v in m.get_full_concentration_dict(y=y_ss).items()}
    y_ss.update(
        {k: m.parameters[k] for k in ("proton_pool_stroma", "NADPH_pool", "NADP_pool")}
    )
    R = 1.987e-3  # kcal / (K * mol)
    T = 298.0  # K
    RT = R * T

    new_parameters = {}

    rxns = [
        ("v2f", "v2r", "k2f", "k2r"),
        ("v3f", "v3r", "k3f", "k3r"),
        ("v4f", "v4r", "k4f", "k4r"),
        ("ALD_DHAP_GAP_FBP_fwd", "ALD_DHAP_GAP_FBP_bwd", "k5f", "k5r"),
        ("ALD_DHAP_E4P_SBP_fwd", "ALD_DHAP_E4P_SBP_bwd", "k8f", "k8r"),
        ("TK_X5P_E4P_F6P_GAP_bwd", "TK_X5P_E4P_F6P_GAP_fwd", "k7f", "k7r"),
        ("TK_X5P_R5P_S7P_GAP_bwd", "TK_X5P_R5P_S7P_GAP_fwd", "k10f", "k10r"),
        ("v11f", "v11r", "k11f", "k11r"),
        ("v12f", "v12r", "k12f", "k12r"),
        ("v14f", "v14r", "k14f", "k14r"),
        ("v15f", "v15r", "k15f", "k15r"),
    ]

    for fwd, bwd, kf, kb in rxns:
        v = v_ss[fwd] - v_ss[bwd]

        Keq = np.exp(-dG0s[fwd] / RT)
        Q = Keq * np.exp(dGs[fwd] / RT)

        substrates = m.rates[fwd]["substrates"].copy()
        products = m.rates[bwd]["substrates"].copy()

        if fwd == "v2f":
            products.append("ADP")
        elif fwd == "v3f":
            substrates.extend(["proton_pool_stroma", "NADPH_pool"])
            products.extend(["NADP_pool", "P_pool"])

        new_parameters[kf] = (v / np.prod([y_ss[s] for s in substrates])) / (
            1 - (Q / Keq)
        )
        new_parameters[kb] = -(v / np.prod([y_ss[p] for p in products])) / (
            1 - (Keq / Q)
        )
    return new_parameters


def qssa_mass_action_1(
    S1: float,
    kr: float,
    keq: float,
) -> float:
    return kr * S1 / keq


def qssa_mass_action_2(
    S1: float,
    S2: float,
    kr: float,
    keq: float,
) -> float:
    return kr * S1 * S2 / keq


def v3f(
    BPGA: float,
    k3f: float,
    NADPH_pool: float,
    proton_pool_stroma: float,
) -> float:
    return k3f * NADPH_pool * BPGA * proton_pool_stroma


def qssa_v3r(
    GAP: float,
    P_i: float,
    k3r: float,
    q3: float,
    NADP_pool: float,
) -> float:
    return k3r * GAP * NADP_pool * P_i / (q3)


def get_poolman_model_split() -> Model:
    m_poolman_split = get_poolman_model()
    m_poolman_split.remove_parameter("k_rapid_eq")
    m_poolman_split.add_parameters(
        {
            "k2f": 8e8,
            "k2r": 8e8,
            "k3f": 8e8,
            "k3r": 8e8,
            "k4f": 8e8,
            "k4r": 8e8,
            "k5f": 8e8,
            "k5r": 8e8,
            "k7f": 8e8,
            "k7r": 8e8,
            "k8f": 8e8,
            "k8r": 8e8,
            "k10f": 8e8,
            "k10r": 8e8,
            "k11f": 8e8,
            "k11r": 8e8,
            "k12f": 8e8,
            "k12r": 8e8,
            "k14f": 8e8,
            "k14r": 8e8,
            "k15f": 8e8,
            "k15r": 8e8,
        }
    )
    m_poolman_split.remove_reactions(
        rate_names=[
            "v2",
            "v3",
            "v4",
            "v5",
            "v7",
            "v8",
            "v10",
            "v11",
            "v12",
            "v14",
            "v15",
        ]
    )

    m_poolman_split.add_reaction(
        rate_name="v2f",
        function=rf.mass_action_2,
        stoichiometry={"PGA": -1, "ATP": -1, "BPGA": 1},
        parameters=["k2f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v2r",
        function=qssa_mass_action_2,
        stoichiometry={"BPGA": -1, "PGA": 1, "ATP": 1},
        modifiers=["ADP"],
        parameters=["k2r", "q2"],
    )

    m_poolman_split.add_reaction(
        rate_name="v3f",
        function=v3f,
        stoichiometry={"BPGA": -1, "GAP": 1},
        parameters=["k3f", "NADPH_pool", "proton_pool_stroma"],
    )

    m_poolman_split.add_reaction(
        rate_name="v3r",
        function=qssa_v3r,
        stoichiometry={"GAP": -1, "BPGA": 1},
        modifiers=["P_pool"],
        parameters=[
            "k3r",
            "q3",
            "NADP_pool",
        ],
    )

    m_poolman_split.add_reaction(
        rate_name="v4f",
        function=rf.mass_action_1,
        stoichiometry={"GAP": -1, "DHAP": 1},
        parameters=["k4f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v4r",
        function=qssa_mass_action_1,
        stoichiometry={"DHAP": -1, "GAP": 1},
        parameters=["k4r", "q4"],
    )

    m_poolman_split.add_reaction(
        rate_name="ALD_DHAP_GAP_FBP_fwd",
        function=rf.mass_action_2,
        stoichiometry={"DHAP": -1, "GAP": -1, "FBP": 1},
        parameters=["k5f"],
    )

    m_poolman_split.add_reaction(
        rate_name="ALD_DHAP_GAP_FBP_bwd",
        function=qssa_mass_action_1,
        stoichiometry={"FBP": -1, "DHAP": 1, "GAP": 1},
        parameters=["k5r", "q5"],
    )

    m_poolman_split.add_reaction(
        rate_name="TK_X5P_E4P_F6P_GAP_bwd",
        function=rf.mass_action_2,
        stoichiometry={"F6P": -1, "GAP": -1, "X5P": 1, "E4P": 1},
        parameters=["k7f"],
    )

    m_poolman_split.add_reaction(
        rate_name="TK_X5P_E4P_F6P_GAP_fwd",
        function=qssa_mass_action_2,
        stoichiometry={"X5P": -1, "E4P": -1, "F6P": 1, "GAP": 1},
        parameters=["k7r", "q7"],
    )

    m_poolman_split.add_reaction(
        rate_name="ALD_DHAP_E4P_SBP_fwd",
        function=rf.mass_action_2,
        stoichiometry={"DHAP": -1, "E4P": -1, "SBP": 1},
        parameters=["k8f"],
    )

    m_poolman_split.add_reaction(
        rate_name="ALD_DHAP_E4P_SBP_bwd",
        function=qssa_mass_action_1,
        stoichiometry={"SBP": -1, "DHAP": 1, "E4P": 1},
        parameters=["k8r", "q8"],
    )

    m_poolman_split.add_reaction(
        rate_name="TK_X5P_R5P_S7P_GAP_bwd",
        function=rf.mass_action_2,
        stoichiometry={"S7P": -1, "GAP": -1, "X5P": 1, "R5P": 1},
        parameters=["k10f"],
    )

    m_poolman_split.add_reaction(
        rate_name="TK_X5P_R5P_S7P_GAP_fwd",
        function=qssa_mass_action_2,
        stoichiometry={"X5P": -1, "R5P": -1, "S7P": 1, "GAP": 1},
        parameters=["k10r", "q10"],
    )

    m_poolman_split.add_reaction(
        rate_name="v11f",
        function=rf.mass_action_1,
        stoichiometry={"R5P": -1, "RU5P": 1},
        parameters=["k11f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v11r",
        function=qssa_mass_action_1,
        stoichiometry={"RU5P": -1, "R5P": 1},
        parameters=["k11r", "q11"],
    )

    m_poolman_split.add_reaction(
        rate_name="v12f",
        function=rf.mass_action_1,
        stoichiometry={"X5P": -1, "RU5P": 1},
        parameters=["k12f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v12r",
        function=qssa_mass_action_1,
        stoichiometry={"RU5P": -1, "X5P": 1},
        parameters=["k12r", "q12"],
    )

    m_poolman_split.add_reaction(
        rate_name="v14f",
        function=rf.mass_action_1,
        stoichiometry={"F6P": -1, "G6P": 1},
        parameters=["k14f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v14r",
        function=qssa_mass_action_1,
        stoichiometry={"G6P": -1, "F6P": 1},
        parameters=["k14r", "q14"],
    )

    m_poolman_split.add_reaction(
        rate_name="v15f",
        function=rf.mass_action_1,
        stoichiometry={"G6P": -1, "G1P": 1},
        parameters=["k15f"],
    )

    m_poolman_split.add_reaction(
        rate_name="v15r",
        function=qssa_mass_action_1,
        stoichiometry={"G1P": -1, "G6P": 1},
        parameters=["k15r", "q15"],
    )
    return m_poolman_split
