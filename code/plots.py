from __future__ import annotations

import math
import warnings
from pathlib import Path
from typing import TypeAlias

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.axes import Axes as Axis
from matplotlib.figure import Figure
from modelbase.ode import Model, Simulator, _LinearLabelSimulate
from modelbase.utils.plotting import plot_grid
from numpy.typing import NDArray

from models.extensions import get_label_compounds
from utils import get_asymmetry_at_label_percentages, get_f6p_asymmetries

# FIXME: matplotlib gives Axes the list[list[Axis]] type
# even though it is an Array. Wait until matplotlib fixes this
Axes: TypeAlias = list[list[Axis]]

TMP_DIR = Path("tmp")
IMG_DIR = TMP_DIR / "img"
FIG_DIR = Path("..") / "paper" / "figures"

TMP_DIR.mkdir(exist_ok=True, parents=True)
IMG_DIR.mkdir(exist_ok=True, parents=True)
FIG_DIR.mkdir(exist_ok=True, parents=True)


def get_carbon_positions(cpd: str) -> list[str]:
    return [f"{cpd}__{i}" for i in range(get_label_compounds()[cpd])]


def get_label_names(cpd: str) -> list[str]:
    return [f"C{i}" for i in range(1, get_label_compounds()[cpd] + 1)]


def crop_figtitle(name: str) -> str:
    if name[0].isnumeric():
        return name.split("-", maxsplit=1)[1]
    return name


def savefig(
    name: str,
    *,
    publish: bool = False,
    show: bool = True,
    overwrite: bool = False,
) -> None:
    if publish:
        file = FIG_DIR / f"{name}.png"
        if not overwrite and file.exists():
            warnings.warn(
                "File already exists. Not overwriting to avoid git trash",
                stacklevel=1,
            )
        else:
            plt.savefig(file, bbox_inches="tight", dpi=300)
    else:
        plt.savefig(IMG_DIR / f"{name}.png", bbox_inches="tight", dpi=100)
    if not show:
        plt.close()


def axs_layout(
    n: int,
    ncols: int | tuple[int, int],
    colwidth: int = 4,
    rowheight: int = 4,
    sharex: bool = True,
    sharey: bool = True,
) -> tuple[Figure, Axes]:
    if not isinstance(ncols, int):
        low, high = ncols
        ncols = min(range(low, high + 1), key=lambda i: n % i)
    nrows = math.ceil(n / ncols)

    fig, axs = plt.subplots(
        nrows,
        ncols,
        figsize=(ncols * colwidth, nrows * rowheight),
        sharex=sharex,
        sharey=sharey,
        squeeze=False,
    )
    return fig, axs  # type: ignore


def plot_label_distribution(df: pd.DataFrame, cpds: list[str]):
    fig, axs = axs_layout(len(cpds), 4)
    for ax, cpd in zip(axs.flatten(), cpds):  # type: ignore
        df.loc[:, get_carbon_positions(cpd)].plot(ax=ax)
        ax.legend(get_label_names(cpd), loc="lower right")
        ax.set_title(cpd)
    fig.tight_layout()
    return fig, axs


def plot_heatmaps(
    scans: dict[str, pd.DataFrame],
    enzymes: dict[str, list[str]],
    factors: NDArray,
    relative_label: float,
    cpd_pairs: list[tuple[str, str]],
    reference_cpd: str,
) -> list[pd.DataFrame]:
    heatmaps: list[pd.DataFrame] = []

    for cpd_pair in cpd_pairs:
        hm = np.full((len(enzymes), len(factors)), np.nan)
        for i, scan in enumerate(scans.values()):
            for j, (_, y) in enumerate(scan.iterrows()):
                hm[i, j] = get_asymmetry_at_label_percentages(
                    y,  # type: ignore
                    [relative_label],  # type: ignore
                    *cpd_pair,
                    reference_cpd,
                )
        heatmaps.append(
            pd.DataFrame(
                hm,
                columns=factors,
                index=list(scans.keys()),
            )
        )
    return heatmaps


def plot_3d_asymmetries(
    scan: pd.DataFrame,
    label_percentages: NDArray,
    xlabel: str = "",
    ylabel: str = "",
    zlabel: str = "",
) -> tuple[Figure, NDArray]:
    scan = scan.dropna()  # Drop failed integrations

    Z = np.array(
        [
            get_f6p_asymmetries(y=dict(v), label_percentages=label_percentages)
            for _, v in scan.iterrows()
        ]
    )  # shape: simulations, simulation step, n_asymmetries

    titles = (
        "F6P C4/C3",
        "F6P C2/C5",
        "F6P C1/C6",
    )
    fig, axs = plt.subplots(1, 3, figsize=[15, 5], subplot_kw={"projection": "3d"})
    for ax, z, title in zip(
        axs.flatten(),
        np.dsplit(Z, 3),
        titles,  # type: ignore
    ):  # shape: simulations, simulation_steps, 1
        z = np.squeeze(z)  # shape simulations, simulation_steps
        df = pd.DataFrame(z, index=scan.index.values)  # type: ignore  # shape: simulations, simulation steps
        # Drop values where interpolation is nan
        df = df.dropna()
        X = df.index.values.reshape(-1, 1) * np.ones(
            len(label_percentages)
        )  # shape: simulations, simulation steps
        Y = (
            label_percentages.reshape(-1, 1) * np.ones(len(X))
        ).T  # shape: simulations, simulation steps
        Z = df.values
        if np.max(Z) > 100:
            Z = np.log(Z)
            title = "log " + title
        ax.plot_surface(X, Y, Z, cmap="rainbow", alpha=0.5)
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
    fig.tight_layout()
    return fig, axs  # type: ignore


def plot_steady_state(m: Model, y: dict[str, float]) -> None:
    s = Simulator(m)
    s.initialise(y)
    _ = s.simulate(100)
    _ = s.plot(
        xlabel="time / s",
        ylabel="Concentration / mM",
        figure_kwargs=dict(figsize=(6, 4)),
    )


def plot_external_phosphate_scan(m: Model, y: dict[str, float]) -> None:
    s = Simulator(m)
    s.initialise(y)
    external_phosphate_scan = s.parameter_scan(
        parameter_name="P_pool_ext",
        parameter_values=np.linspace(0.1, 2, 128),
    )
    results = external_phosphate_scan.fillna(0)
    results["BPGA"] *= 50
    plot_groups = [
        ["G6P", "PGA", "F6P", "S7P"],
        ["RU5P", "X5P", "SBP", "G1P"],
        ["ATP", "DHAP", "RUBP", "R5P"],
        ["BPGA", "E4P", "FBP", "GAP"],
    ]
    legend_groups = [
        ["G6P", "PGA", "F6P", "S7P"],
        ["RU5P", "X5P", "SBP", "G1P"],
        ["ATP", "DHAP", "RUBP", "R5P"],
        ["50 x BPGA", "E4P", "FBP", "GAP"],
    ]
    ext_p = results.index.values
    _ = plot_grid(
        plot_groups=[(ext_p, results.loc[:, i].values) for i in plot_groups],
        legend_groups=legend_groups,
        ncols=2,
        sharex=True,
        sharey=False,
        xlabels="External orthophosphate [mM]",
        ylabels="Concentration [mM]",
        figure_title="External orthophosphate scan",
    )


def plot_distribution_difference(data: pd.DataFrame, model: str, cpds: list[str]):
    fig, axs = axs_layout(len(cpds), (3, 5))
    fig.suptitle(model)
    for ax, cpd in zip(axs.flatten(), cpds):  # type: ignore
        data.loc[:, get_carbon_positions(cpd)].plot(ax=ax)
        ax.legend(get_label_names(cpd), loc="lower right")
        ax.set_title(cpd)
        savefig(f"diff-{model}")


def plot_all_label_distributions(
    self: _LinearLabelSimulate,
    ncols: int = 4,
) -> Figure:
    all_cpds = [
        # "PGA",
        # "BPGA",
        "GAP",
        # "DHAP",
        "FBP",
        "F6P",
        "E4P",
        # "G6P",
        # "G1P",
        "SBP",
        "S7P",
        # "X5P",
        "R5P",
        "RU5P",
        "RUBP",
        "A5P",
        "OBP",
        "O8P",
    ]
    time = self.get_time()
    plot_groups = [
        (time, self.get_label_distribution(compound=compound))
        if compound in self.model.isotopomers
        else list()
        for compound in all_cpds
    ]
    legend_groups = [
        self._make_legend_labels("C", compound, 1)
        if compound in self.model.isotopomers
        else list()
        for compound in all_cpds
    ]

    return plot_grid(
        plot_groups=plot_groups,  # type: ignore
        legend_groups=legend_groups,
        ncols=ncols,
        sharex=True,
        sharey=True,
        plot_titles=all_cpds,
        xlabels="",
        ylabels="Relative concentration",
        grid=True,
        tight_layout=True,
    )[0]
