from __future__ import annotations

import itertools as it
import multiprocessing
import sys
from concurrent import futures
from typing import Any, Callable, Collection, Hashable, Iterable, TypeAlias, TypeVar

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pebble
from cycler import Cycler, cycler
from matplotlib.axes import Axes as Axis
from modelbase.ode import LinearLabelModel, Model, Simulator
from numpy.typing import NDArray
from tqdm import tqdm

T = TypeVar("T")
Tin = TypeVar("Tin")
Tout = TypeVar("Tout")
T1 = TypeVar("T1")
T2 = TypeVar("T2")
Ti = TypeVar("Ti", bound=Iterable)
Thash = TypeVar("Thash", bound=Hashable)

# FIXME: matplotlib gives Axes the list[list[Axis]] type
# even though it is an Array. Wait until matplotlib fixes this
Axes: TypeAlias = list[list[Axis]]


def test_versions() -> None:
    from platform import python_version

    from packaging.version import Version  # type: ignore
    from pkg_resources import get_distribution  # type: ignore

    assert Version(python_version()) >= Version("3.11.0")
    assert Version(get_distribution("modelbase").version) >= Version("1.25.1")


def get_prop_cycle() -> Cycler:
    orig_cycler = plt.rcParams["axes.prop_cycle"].by_key()["color"]  # type: ignore
    return cycler("color", orig_cycler) + cycler(
        "linestyle", ["-", "-.", "-", "-.", "-", "-.", "-", "-."]
    )


def _parameter_scan(
    factor: float,
    parameter_names: list[str],
    base_model: Model,
    label_model: LinearLabelModel,
    y0: dict[str, float],
    label_y0: dict[str, float],
    label_sim_kwargs: dict[str, Any],
) -> tuple[float, dict[str, NDArray]]:
    m = base_model.copy()
    lm = label_model.copy()

    m.update_parameters(
        parameters={i: m.parameters[i] * factor for i in parameter_names}
    )

    s = Simulator(m)
    s.initialise(y0=y0, test_run=False)
    _, y = s.simulate_to_steady_state()
    if y is None:
        return (
            factor,
            dict(
                zip(label_model.compounds, np.full(len(label_model.compounds), np.nan))
            ),
        )

    y_ss: dict[str, float] = dict(zip(m.compounds, y[-1]))
    v_ss: dict[str, float] = dict(m.get_fluxes_df(y_ss).iloc[0])

    ls = Simulator(lm)
    ls.initialise(label_y0=label_y0, y_ss=y_ss, v_ss=v_ss)
    _, y = ls.simulate(**label_sim_kwargs)
    if y is None:
        return (
            factor,
            dict(
                zip(label_model.compounds, np.full(len(label_model.compounds), np.nan))
            ),
        )
    return factor, dict(zip(label_model.compounds, y.T))


def parameter_scan(
    parameter_names: list[str],
    factors: NDArray,
    base_model: Model,
    label_model: LinearLabelModel,
    y0: dict[str, float],
    label_y0: dict[str, float],
    label_sim_kwargs: dict[str, Any] = {"time_points": np.geomspace(1e-18, 1000, 1000)},
) -> pd.DataFrame:
    with futures.ProcessPoolExecutor() as executor:
        results = dict(
            executor.map(
                _parameter_scan,
                factors,
                it.repeat(parameter_names),
                it.repeat(base_model),
                it.repeat(label_model),
                it.repeat(y0),
                it.repeat(label_y0),
                it.repeat(label_sim_kwargs),
            )
        )
    return pd.DataFrame(results).T


def get_asymmetry_at_label_percentages(
    y: dict[str, NDArray],
    label_percentages: NDArray,
    cpd1: str,
    cpd2: str,
    reference_cpd: str,
) -> NDArray:
    if np.any(np.isnan(y[reference_cpd])):
        return np.full(len(label_percentages), np.nan)
    y_interp = {
        i: np.interp(
            x=label_percentages, xp=y[reference_cpd], fp=y[i], left=np.nan, right=np.nan
        )
        for i in (cpd1, cpd2)
    }
    return y_interp[cpd1] / y_interp[cpd2]


def get_f6p_asymmetries(
    y: dict[str, NDArray],
    label_percentages: NDArray,
) -> NDArray:
    """Return interpolated asymmetries based on label percentage of F6P4

    Returns
    -------
    C4/C3
    C2/C5
    C1/C6

    """
    return np.array(
        [
            get_asymmetry_at_label_percentages(
                y=y,
                label_percentages=label_percentages,
                cpd1="F6P__3",
                cpd2="F6P__2",
                reference_cpd="F6P__3",
            ),
            get_asymmetry_at_label_percentages(
                y=y,
                label_percentages=label_percentages,
                cpd1="F6P__1",
                cpd2="F6P__4",
                reference_cpd="F6P__3",
            ),
            get_asymmetry_at_label_percentages(
                y=y,
                label_percentages=label_percentages,
                cpd1="F6P__0",
                cpd2="F6P__5",
                reference_cpd="F6P__3",
            ),
        ]
    ).T


def get_enzyme_parameters() -> dict[str, list[str]]:
    return {
        "RuBisCO": ["Vmax_1"],
        "PGK": ["k2f", "k2r"],
        "GADPH": ["k3f", "k3r"],
        "TPI": ["k4f", "k4r"],
        "ALD": ["k5f", "k5r", "k8f", "k8r"],
        "FBPase": ["Vmax_6"],
        "TK": ["k7f", "k7r", "k10f", "k10r"],
        "SBPase": ["Vmax_9"],
        "RPI": ["k11f", "k11r"],
        "RPE": ["k12f", "k12r"],
        "PRK": ["Vmax_13"],
        "GPI": ["k14f", "k14r"],
        "PGM": ["k15f", "k15r"],
    }


def get_y_ss_poolman() -> dict[str, float]:
    return {
        "PGA": 0.6437280277346407,
        "BPGA": 0.001360476366780556,
        "GAP": 0.011274125311289358,
        "DHAP": 0.24803073890728228,
        "FBP": 0.019853938009873073,
        "F6P": 1.0950701164493861,
        "E4P": 0.028511831060903877,
        "G6P": 2.5186612678035734,
        "G1P": 0.14608235353185037,
        "SBP": 0.09193353265673603,
        "S7P": 0.23124426886012006,
        "X5P": 0.036372985623662736,
        "R5P": 0.06092475016463224,
        "RUBP": 0.24993009253928708,
        "RU5P": 0.02436989993734177,
        "ATP": 0.43604115800259613,
    }


def get_y_ss_bassham() -> dict[str, float]:
    return {
        "PGA": 1.4,
        "BPGA": 0.001,
        "GAP": 0.032,
        "DHAP": 0.64,
        "FBP": 0.097,
        "F6P": 0.53,
        "G6P": 0.73,
        "G1P": 0.33,
        "SBP": 0.114,
        "S7P": 0.248,
        "E4P": 0.02,
        "X5P": 0.021,
        "R5P": 0.034,
        "RU5P": 0.012,
        "RUBP": 2.04,
        "ATP": 0.39,
    }


def default_if_none(el: T | None, default: T) -> T:
    return default if el is None else el


def parallelise(
    fn: Callable[[Tin], tuple[Tin, Tout]],
    inputs: Collection[Tin],
    *,
    max_workers: int | None = None,
    timeout: float | None = None,
    disable_tqdm: bool = False,
    tqdm_desc: str | None = None,
) -> dict[Tin, Tout]:
    results: dict[Tin, Tout]
    if sys.platform in ["win32", "cygwin"]:
        results = dict(
            tqdm(
                map(fn, inputs),
                total=len(inputs),
                disable=disable_tqdm,
                desc=tqdm_desc,
            )
        )
    else:
        results = {}
        max_workers = default_if_none(max_workers, multiprocessing.cpu_count() - 1)

        with tqdm(
            total=len(inputs),
            disable=disable_tqdm,
            desc=tqdm_desc,
        ) as pbar, pebble.ProcessPool(max_workers=max_workers) as pool:
            future = pool.map(fn, inputs, timeout=timeout)
            it = future.result()
            while True:
                try:
                    key, value = next(it)
                    pbar.update(1)
                    results[key] = value
                except StopIteration:  # noqa: PERF203
                    break
                except TimeoutError:
                    pbar.update(1)
    return results
