\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{biorxiv}

% Use existing stuff
\LoadClass{article}

% Layout
\RequirePackage[
    paper=a4paper,
    inner=2cm, % Inner margin
    outer=2cm, % Outer margin
    bindingoffset=0cm, % Binding offset
    top=2.5cm, % Top margin
    bottom=2.5cm, % Bottom margin
    % showframe, % Uncomment to show how the type block is set on the page
]{geometry}
\setlength{\columnsep}{7mm}

% https://aty.sdsu.edu/bibliog/latex/floats.html
\renewcommand{\topfraction}{0.9}% max fraction of floats at top
\renewcommand{\bottomfraction}{0.8}% max fraction of floats at bottom
\setcounter{bottomnumber}{2}
\setcounter{totalnumber}{4}% 2 may work better
\setcounter{dbltopnumber}{2}% for 2-column pages
% Parameters for FLOAT pages (not text pages):
\renewcommand{\floatpagefraction}{.8}% only pages with more than 80% of floats, will become pure float-only pages
\renewcommand{\floatpagefraction}{0.7}	% require fuller float pages
\renewcommand{\dblfloatpagefraction}{0.7}	% require fuller float pages
% N.B.: floatpagefraction MUST be less than topfraction !!

% Text
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}% Encode file as UTF8
\RequirePackage[english]{babel} % correct hyphenation
\RequirePackage{csquotes}% Properly format quotes
\RequirePackage[version=4]{mhchem}% Chemical typesetting
\RequirePackage{xcolor}
\RequirePackage{titlesec}
\RequirePackage{microtype}% Adjust hyphenation etc
\RequirePackage{ragged2e}% Hyphenation in ragged envs
% \renewcommand{\familydefault}{\sfdefault}

\definecolor{light-gray}{gray}{0.6}
\RequirePackage[left]{lineno}
\renewcommand\linenumberfont{\normalfont\tiny\sffamily\color{light-gray}}

% Paragraph formatting
\setlength{\parindent}{0pt}% Remove paragraph indent
% \RequirePackage[skip=1em]{parskip}% Insert linebreak between paragraphs
\RequirePackage[
    singlespacing
    % onehalfspacing
    % doublespacing
]{setspace} %  distance between lines
\RequirePackage[section]{placeins}

% Authors and affiliation
\RequirePackage{authblk}

% Placeholders
\RequirePackage{lipsum}

% References
\RequirePackage{varioref}% \vref
\RequirePackage{hyperref}
\RequirePackage[capitalize]{cleveref}% \cref
\RequirePackage[
    style=numeric,
    sorting=none,
    sortcites=true,
    backend=biber
]{biblatex}


\RequirePackage{xurl}

\urlstyle{rm}

% Create custom counter for section such that supplementary sections
% can work with hyperref
\newcounter{hsection}
\newcounter{htable}
\newcounter{hfigure}
\newcounter{hthepage}

\makeatletter
\@addtoreset{thepage}{hthepage}
\@addtoreset{section}{hsection}
\@addtoreset{table}{htable}
\@addtoreset{figure}{hfigure}
\makeatother


% Captions
\RequirePackage[
    font=small,
    labelfont=bf
]{caption} % Bolds Caption type and decreases caption font size
\RequirePackage{subcaption}
\RequirePackage[all]{hypcap}% adjust anchors of captions

% Tables
\RequirePackage{multicol}
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage{booktabs}

% Images
\RequirePackage{graphicx}
\makeatletter
\def\maxwidth#1{\ifdim\Gin@nat@width>#1 #1\else\Gin@nat@width\fi}
\makeatother

% Math
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{array}
\RequirePackage{mathtools}
\RequirePackage{siunitx}
\RequirePackage{breqn}% Automatic line breaking of equations

% Misc
\RequirePackage{orcidlink}%Load after hyperref and tikz
\RequirePackage{glossaries}
% \RequirePackage{minted}% Syntax highlighting
\RequirePackage{pdfpages}% Include external pdf pages
\RequirePackage{ifthen}% Conditionals in macros
\RequirePackage{xstring}% For StrLen etc
\RequirePackage{attachfile}% For StrLen etc



%----------------------------------------------------------------------------------------
%	Fancy header and footer
%----------------------------------------------------------------------------------------
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\newcommand{\leadauthor}[1]{\def\@leadauthor{#1}}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot[R]{{\footnotesize
            \@leadauthor{} et. al%
            \hspace{7pt}|\hspace{7pt}\includegraphics[width=1cm, trim=0 1.5cm 0 0]{bioRxiv_logo.png}%
            \hspace{7pt}|\hspace{7pt}\today%
            \hspace{7pt}|\hspace{7pt}Page \thepage\textendash\pageref{page:bib}
            % This one fails if we directly add the supplementaries to it
            % \hspace{7pt}|\hspace{7pt}Page \thepage\textendash\pageref{LastPage}
        }}


%----------------------------------------------------------------------------------------
%	Fancy header and footer
%----------------------------------------------------------------------------------------
% \titlespacing{command}{left spacing}{before spacing}{after spacing}[right]
\titlespacing{\section}{0pt}{6pt plus 4pt minus 2pt}{2pt plus 2pt minus 2pt}
\titlespacing{\subsection}{0pt}{6pt plus 4pt minus 2pt}{2pt plus 2pt minus 2pt}
\titlespacing{\subsubsection}{0pt}{6pt plus 4pt minus 2pt}{2pt plus 2pt minus 2pt}

\titleformat{\section}{\normalfont\Large\bfseries}{}{0em}{}

\newcommand{\beginmain}{%
    \twocolumn
    % \onecolumn
    \sloppy% avoid overflow into second column
    \titleformat{\section}{\normalfont\Large\bfseries}{}{0em}{}
    \titleformat{\subsection}{\normalfont\large\bfseries}{}{0em}{}
}

\newcommand{\beginsupplement}{%
    \onecolumn%
    \fussy% turn off \sloppy
    \titleformat{\section}{\normalfont\Large\bfseries}{\thesection}{1em}{}%
    \stepcounter{hsection}%
    \stepcounter{htable}%
    \stepcounter{hfigure}%
    \stepcounter{hpage}% not hthepage, otherwise supplementaries aren't build
    \renewcommand{\thesection}{S\arabic{section}}%
    \renewcommand{\thetable}{S\arabic{table}}%
    \renewcommand{\thefigure}{S\arabic{figure}}%
    \fancyfoot[R]{{\footnotesize
                \@leadauthor{} et. al%
                \hspace{7pt}|\hspace{7pt}\includegraphics[width=1cm, trim=0 1.5cm 0 0]{bioRxiv_logo.png}%
                \hspace{7pt}|\hspace{7pt}\today%
                \hspace{7pt}|\hspace{7pt}Supplementary material%
                % FIXME: what is the start and end page here???
            }}
}

% usage: \fixme{}
% usage: \fixme{reason}
\newcommand{\fixme}[1]{%
\StrLen{#1}[\MyStrLen]%
\ifthenelse{ \MyStrLen > 1 }%
{{\color{red} FIXME: {#1}}}%
{{\color{red} FIXME}}%
}

% Notes
% titleformat
% \titleformat{⟨command ⟩}[⟨shape⟩]{⟨format⟩}{⟨label ⟩}{⟨sep⟩}{⟨before-code⟩}[⟨after-code⟩]

% sectioning for book
% \frontmatter
% \mainmatter
% \appendix
% \backmatter
