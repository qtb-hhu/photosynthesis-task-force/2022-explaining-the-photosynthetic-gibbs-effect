% Abstract
\textbf{%
The photosynthetic Gibbs effect describes an asymmetric distribution of radioactive labels in sugars produced by photosynthesis.
Previous arguments regarding whether these findings are in agreement with the Calvin-Benson-Bassham cycle have mostly been qualitative.
Here we present a quantitative analysis of the photosynthetic Gibbs using a mathematical model of the Calvin-Benson-Bassham cycle.
We show that the photosynthetic Gibbs effect is in agreement with the Calvin-Benson-Bassham cycle, providing a more detailed explanation as to why.
}

\medskip

\textbf{Correspondence}: \url{oliver.ebenhoeh@hhu.de}\\
\textbf{Code}: \url{https://gitlab.com/qtb-hhu/photosynthesis-task-force/2022-explaining-the-photosynthetic-gibbs-effect}\\


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}\label{introduction}
\begin{linenumbers}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Topic: CBB is important
The \gls{cbb} cycle is a critical biochemical pathway integral to global carbon fixation \cite{raven2009contributions}.
Various photosynthetic organisms, including plants, utilize this cycle to assimilate carbon dioxide, ultimately converting it into sugars \cite{bassham1960path}.
Central to this process is the enzyme \gls{rubisco}, which catalyzes the carboxylation of ribulose-1,5-bisphosphate.
The enzyme \gls{rubisco} is arguably the most abundant enzyme on Earth, responsible for over 99 \% of global carbon dioxide fixation \cite{Ellis1979,raven2013rubisco}.
Given its pivotal role, the biochemical mechanisms underlying inorganic carbon fixation have been the focus of extensive scientific research since the inception of metabolic studies.

% Topic: carbon labels were used to understand CBB
In the initial phase of elucidating the \gls{cbb} cycle, Otto Kandler and Martin Gibbs experimentally tested Melvin Calvin's proposed carbon fixation model by employing radioactive carbon labeling.
Unexpectedly, their findings revealed an asymmetric distribution of radioactive labels within starch-derived hexoses produced during photosynthesis.
This asymmetry was consistent across various experimental conditions and independent of the botanical origin of the hexoses.

% Topic: gibbs-effect
The observed asymmetries, now referred to as the 'Gibbs effect', demonstrated a specific labeling sequence in starch-derived hexoses: the fourth carbon position consistently acquired the radioactive label prior to the third, the first position was labeled before the sixth, and the second carbon was labeled before the fifth.
In the following, these asymmetries will be abbreviated by a fraction, with the numerator showing the position labeled first, e.g. \fourthree{}.

% Topic: Novelty of our work
In the debate regarding whether these findings are in agreement with the \gls{cbb} cycle reaction scheme, the arguments have largely been qualitative and speculative in nature \cite{Ebenhoh2018}.
Thus, we present a quantitative examination of the photosynthetic Gibbs effect, building on previous work \cite{Ebenhoh2018}.
This not only gives us the ability to give an explanation for the patterns observed by Gibbs, but also to make falsifiable predictions about the label distribution of all other compounds in the CBB cycle.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Gibbs effect readily explained by CBB cycle structure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}[htb]
    \centering{}
    \includegraphics[width=0.8\linewidth]{schemes/full.png}
    \caption{%
    The path of carbon atoms in the CBB cycle.
    Shown are the carbon positions of CBB cycle intermediates and the mappings of the corresponding reactions.
    Reactions changing the label position between the reactants are coloured, irreversible reactions are drawn with dashed lines.
    Triose phosphate exporters, pathways to starch production and enzyme names are omitted for clarity.}
    \label{fig:scheme}
\end{figure*}

\begin{figure}
    \centering{}
    \includegraphics[width=0.99\linewidth]{figures/initial-labels.png}
    \caption{%
        Time development of label distribution in \acrshort{fbp} and \acrshort{f6p} shown relative to the concentration of the respective metabolite.
        C1 to C6 denote the respective carbon atoms.
    }
    \label{fig:initial-labels}
\end{figure}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/enzyme-scan.png}
    \caption{%
        Systematic scan of relative F6P label asymmetries dependent on the activity of selected \gls{cbb} cycle enzymes.
        The asymmetries are calculated as the integral of the difference between the respective time series and shows relative to the wildtype asymmetries.
        The x-axis shows the scaling factor of the $V_{\mathrm{max}}$ and the y-axis denotes the respective enzyme.
    }
    \label{fig:enzyme-scan}
\end{figure*}

% Topic: introduction to isotope-labeled models
To quantitatively study the Gibbs effect with theoretical models, we implemented and adapted a published kinetic model of the CBB cycle \cite{Pettersson1988,Poolman2000}.
We then transformed the model to reflect all possible isotopomers using the Python package modelbase \cite{modelbase}.
For this, every intermediate is represented by all possible $2^n$ isotopomer versions, where $n$ denotes the number of carbon atoms.
Likewise, all reactions are represented by all possible versions transforming the corresponding isotopomers.
The resulting scheme is depicted in \cref{fig:scheme}.

% Topic: rapid equilibrium assumption
Due to the rapid equilibrium assumption of the fast reversible reactions in the model showed an instant equilibration of the labels in the respective intermediates, see supplementary \cref{fig:initial-labels-instant-equilibration}.
Thus, in contrast to the original model, we here explicitly determined the disequilibrium of the involved reactions.
This involved the identification of forward and backward rate constants of the reversible reactions.
The forward and backward rate constants can be uniquely determined from measured concentrations and the equilibrium constants \cite{Bassham1969,equilibrator}.

% Topic: model reproduces gibbs effect
With this we performed simulations over time, assuming a constant supply of fully labeled carbon dioxide.
To avoid biases by arbitrary choices of time points to compare the asymmetry, we took the integral of the difference between the labelled carbons as a measurement of the asymmetry.

\cref{fig:initial-labels} shows the labeling patterns of \gls{fbp} and \gls{f6p} (see supplementary \cref{fig:initial-labels-all} for all other metabolites).
In both hexoses the fourth carbon is labeled before the third.
However, while in \acrshort{f6p} the first carbon is labeled before the sixth and the second carbon before the fifth, this pattern is reversed in \acrshort{fbp}.
The label asymmetries in \gls{f6p} originally described in experiments by Gibbs can be observed without any further parameter adjustments to the model.

% Topic: which enzymes change degree of asymmetry and how much
In \gls{fbp} however, the \onesix{} and \twofive{} asymmetries are reversed.
To investigate what causes the contrast between the \onesix{} and \twofive{} label asymmetries in \gls{fbp} and \gls{f6p}, we systematically varied the enzyme activity of all \gls{cbb} cycle enzymes.
We then calculated the respective label asymmetries for \gls{f6p}, shown in \cref{fig:enzyme-scan} for a selection of enzymes (see supplementary \cref{fig:enzyme-scan-full} for all \gls{cbb} cycle enzymes).
The \fourthree{} asymmetry in \gls{f6p} is mostly affected by \gls{tpi}, which shows an antiproportional effect.
For example, at half the \gls{tpi} activity the \fourthree{} asymmetry is 150 \% relative to the wildtype and at twice the \gls{tpi} acitivity the asymmetry is 57 \%.
Increasing \gls{sbpase} also slightly decreases the \fourthree{} asymmetry, which at fivefold activity is at 89 \% of the wildtype asymmetry.

% C2/C5 and C1/C6
As with the \fourthree{} asymmetry, an increase in \gls{tpi} activity also decreases the \onesix{} and \twofive{} asymmetries.
This is also the case for \gls{sbpase} and \gls{prk}, which reduce the \twofive{} and \onesix{} asymmetries.
At fivefold \gls{sbpase} activity both \twofive{} and \onesix{} asymmetries are at 31 \% of the wildtype activity.
Similarly, at fivefold \gls{prk} activity both asymmetries are at 16 \% of the wildtype asymmetry.
Contrastingly, increasing \gls{tk} increases the \onesix{} and \twofive{} asymmetries, which at double the activity are at 200 \% of their respective wildtype asymmetry.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Octuloses might influence labeling patterns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}
    \begin{tabular*}{\linewidth}{@{\extracolsep{\fill}} cc}%
        \multicolumn{2}{c}{\textbf{Non-standard transketolase reactions}}\\
        \hline\addlinespace[1ex]
        \textbf{Substrates} & \textbf{Products} \\
        \acrshort{f6p} + \acrshort{r5p} & \acrshort{s7p} + \acrshort{e4p} \\
        \acrshort{x5p} + \acrshort{g6p} & \acrshort{o8p} + \acrshort{gap} \\
        \acrshort{f6p} + \acrshort{g6p} & \acrshort{o8p} + \acrshort{e4p} \\
        \acrshort{s7p} + \acrshort{g6p} & \acrshort{o8p} + \acrshort{r5p} \\

        \addlinespace[1ex]
        \multicolumn{2}{c}{\textbf{Neutral transketolase reactions}}\\
        \hline\addlinespace[1ex]
        \textbf{Substrates} & \textbf{Products} \\
        \acrshort{x5p} + \acrshort{gap} & \acrshort{x5p} + \acrshort{gap} \\
        \acrshort{f6p} + \acrshort{e4p} & \acrshort{f6p} + \acrshort{e4p} \\
        \acrshort{s7p} + \acrshort{r5p} & \acrshort{s7p} + \acrshort{r5p} \\
        \acrshort{o8p} + \acrshort{g6p} & \acrshort{o8p} + \acrshort{g6p} \\

        \addlinespace[1ex]
        \multicolumn{2}{c}{\textbf{Non-standard aldolase reactions}}\\
        \hline\addlinespace[1ex]
        \textbf{Substrates} & \textbf{Products} \\
        \acrshort{dhap} + \acrshort{a5p} & \acrshort{obp} \\
    \end{tabular*}
    \caption{%
        Stoichiometries of extended \gls{tk} and \gls{ald} reactions.
    }
    \label{table:reactions}
\end{table}


\begin{figure}
    \centering{}
    \includegraphics[width=\linewidth]{figures/asymmetries-tk-ald.png}
    \caption{%
        Relative \gls{f6p} asymmetries dependent on the rate constant the extended \gls{tk} reactions (upper panel) or both \gls{tk} and \gls{ald} reactions (lower panel).
        Rate constants are shows relative to to the respective rate constant of the standard \gls{cbb} cycle reaction.
    }
    \label{fig:asymmetries-tk-ald}
\end{figure}


\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/extended-tk-ald-subset.png}
    \caption{%
        Effect of extended \gls{tk} and \gls{ald} on the labeling pattern of selected \gls{cbb} cycle intermediates.
        The effect is shown as the integral of the difference between the time course of the respective carbon at a given relative rate constant against the time course at wildtype conditions.
        The rate constants are shown relative to the value of their respective wildtype reaction rate constants.
    }
    \label{fig:scan-tk-ald-subset}
\end{figure*}

% Topic: Extension of the model
The model in its current form readily explains the photosynthetic Gibbs effect.
However, there is a long standing debate about additional reactions catalysed by the promiscuous enzymes transketolase and \gls{ald} \cite{sharkey2021pentose}.
These additional reactions allow production of \gls{o8p}, \gls{obp} and \gls{a5p} \cite{clark1971transketolase,horecker1982occurrence}.
All these metabolites have been detected experimentally in both photosynthetic and heterotrophic organisms \cite{williams2006metabolic,flanigan2006re}.
However, the significance of the flux through the reactions producing those metabolites is still debated \cite{stincone2015return}.
We thus sought to quantify the effect those reactions might have on the labeling distribution.
For this, we included the reactions in \cref{table:reactions}.
This included ways to produce \acrshort{a5p} or consume \acrshort{obp} further for which we included an \acrshort{a5p} isomerase which catalyses the reaction \acrshort{a5p} <=> \acrshort{ru5p} and an \gls{obpase}, which catalyzes the reaction \acrshort{obp} + \acrshort{h2o} <=> \acrshort{o8p} + \acrshort{pi}.
The respective rate equations of \gls{tk} and \gls{ald} were adapted using reversible kinetics to ensure thermodynamically sound behaviour \cite{equilibrator}.
We assumed \gls{obpase} to be irreversible and thus adapted the rate equation of \gls{sbpase} instead.
For all the newly added reactions we added scaling factors, as we could not find any kinetic data for them.

% Topic systematic TK & ALD scan on asymmetry
We systematically scanned the label distributions for model variants containing the extended \gls{tk} reactions, the extended \gls{ald} reactions or both.
The \onesix{} and \twofive{} asymmetries are increased more than 200 \% if the relative activity of the extended transketolase reactions matches the one for the standard ones, see upper panel of \cref{fig:asymmetries-tk-ald}.
If the extended \gls{ald} reaction is also scaled proportionally, the \fourthree{} asymmetry increases to over 300 \% and the \onesix{} asymmetry is also further increased, see lower panel of \cref{fig:asymmetries-tk-ald}.

% Topic: effect on remaining metabolites
As the inclusion of extended \gls{tk} and \gls{ald} reactions can increase the Gibbs effect we sought to further quantify the general effect on the label distribution in the \gls{cbb} cycle.
The effect of the extended \gls{tk} and \gls{ald} reactions on the labeling patterns of select \gls{cbb} cycle intermediates is shown in \cref{fig:scan-tk-ald-subset} systematically for rate constants between 0-100 \% of the respective wildtype rate constants
(see supplementary \cref{fig:scan-tk-ald-full} for all \gls{cbb} cycle intermediates).
There we took the integral of the relative difference between the time course at a specific relative rate constant and the one at zero activity as a metric for the change of a given label.
Even at 100 \% relative rate constant of the extended \gls{tk} and \gls{ald} reactions, the magnitude of the relative difference is less than 5 \% (see \gls{g1p} in supplementary \cref{fig:scan-tk-ald-full}).

% Topic: carbons with shorter pathways
Of special interest are the four carbons in the \gls{cbb} cycle which with the additional reactions require fewer enzymatic steps to be labelled.
For example, with the addition of the extended reactions the first carbon of \gls{g6p} only requires three instead of seven enzymatic steps to be labelled starting from \gls{dhap}.
This can be achieved by the reaction sequence of \gls{ald} producing \gls{obp}, OBPase producing \gls{o8p} and \gls{tk} running backwards.
Similar pathways can be found for the first carbon of \gls{f6p}, requiring seven instead of nine steps, the second carbon of \gls{g6p}, requiring 11 instead of 13 steps and the first carbon of \gls{s7p}, requiring eight instead of nine steps.
At 100 \% relative rate constant the first carbon of \gls{g6p} is labelled 1 \% more.
Similarly, the first carbon of \gls{f6p} is labelled 0.5 \% more and the label of the second carbon of \gls{g6p} and the first carbon of \gls{s7p} are also increased.

% Further effects
However, these are not the only effects the addition of the extended reaction has on the label distribution.
For a low relative rate constant, the third carbon of \gls{pga} is labelled less, which is alleviated at high relative activitiy by \gls{ald} activity (see supplementary \cref{fig:scan-tk-full} for a comparison to just the \gls{tk} activity).
This effect propagates to the fourth and third carbon of \gls{e4p}, the sixth and seventh carbon of the heptuloses and the fifth and fourth carbon of the pentoses respectively.
Similarly, the increased \fourthree{} asymmetry in \gls{fbp} can be observed in the second and first carbon of \gls{e4p}.

At a high relative rate constant the third carbon of \gls{g6p} the change in the label of the third carbon is more negative than the one of \gls{f6p}.
The third carbon of \gls{g6p} can only be derived from the third carbon of \gls{f6p} or the second carbon of \gls{a5p}.
This suggests that the same mechanism causing the increase in the labeling of the first carbon decreases the labeling of this carbon.

% Topic: S7P vs SBP
Carbons three and four of \gls{sbp} and \gls{s7p} are affected differently by an increased relative rate constant of the extended reactions.
While there is almost no change in those carbons in \gls{sbp} (see supplementary \cref{fig:scan-tk-ald-full}), they are reduced by roughly 2 \% in \gls{s7p}.
In the extended \gls{tk} reactions the third and fourth \gls{s7p} carbons are derived from the first and second carbons of \gls{r5p}, which also happens in the standard \gls{tk} reactions.
Thus, this effect is caused by an increase in transketolase activity in general and not by the extended reactions per se.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Discussion \& Conclusions}\label{discussion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Topic: model qualitatively reproduces Gibbs data
Our model, without any parameter fitting, qualitatively reproduces the asymmetries experimentally measured by Gibbs.
That is, we can observe that in \gls{f6p} the fourth carbon is labeled before the third, the first before the sixth and the second before the fifth, see \cref{fig:initial-labels}.

% Topic: C4C3 asymmetry
Bassham noted in 1964 that the asymmetry in the 4 and 3 positions can be explained by the different pool sizes of the triose phosphates \cite{Bassham1964}.
We argue here that regardless of the pool sizes the \fourthree{} asymmetry will always be observed.
It is true that \gls{tpi} activity and thus different pool sizes of \gls{gap} and \gls{dhap} can change the magnitude of the asymmetry.
However, the set of reactions necessary to label the fourth carbon of \gls{fbp} is a strict subset of the reactions necessary to label the third carbon of \gls{fbp}.
As the relative outflux of a label from one metabolite cannot exceed the relative amount of label in the respective pool, it is thus structurally impossible for the third carbon to be labeled more than the fourth.

% Topic: FBP asymmetries
While the \fourthree{} asymmetry is present in \gls{fbp} as well, the \onesix{} and \twofive{} asymmetries are reversed.
This difference can readily be explained by \gls{tk} activity, as Bassham already noted in 1964 \cite{Bassham1964}.
In order for the first and sixth carbon of \gls{fbp} to be labeled, the third carbon of \gls{gap} needs to be labeled.
If \gls{co2} is the only label influx to the system, this requires the label to be passed through the entire cycle once, via the third carbon of \gls{sbp}, which ends up in the third carbon of \gls{gap}.
The difference between the labeling of the first and sixth position can then be explained exactly like the \fourthree{} asymmetry above.

% Topic: F6P asymmetries
In \gls{f6p} however, there exists a second influx route for labels into the first two carbons via \gls{tk}.
This just requires six reactions from \gls{dhap} to take place, while the first label in \gls{fbp} requires ten reactions of which four are shared with the alternative route.
As \cref{fig:enzyme-scan} shows, increased \gls{tk} activity increases the \onesix{} label asymmetry, giving more weight to this explanation.
While this asymmetry in principle can be reversed by very low \gls{tk} activity the model does not yield stable solutions for those cases.
An analogous explanation can be given for the \twofive{} asymmetry.

% Topic: octuloses
We next extended the model to include non-standard \gls{tk} and \gls{ald} reactions, capable of producing octuloses.
While octuloses have been detected experimentally, the significance of the flux through them still debated \cite{williams2006metabolic,flanigan2006re}.
As kinetic parameters for these reactions are unknown we systematically scanned possible rate constants between 0-100 \% of the rate constants of the respectively standard \gls{tk} and \gls{ald} reactions.
The inclusion of extended \gls{tk} and \gls{ald} reactions increased the magnitude of the Gibbs effect by up to three-fold, see \cref{fig:asymmetries-tk-ald}.
However, when the relative changes of all the labels in the \gls{cbb} cycle was assessed, we found no change larger than $\pm$ 5 \% (\cref{fig:scan-tk-ald-subset}).
This suggests both that small changes in labelling can lead to large changes in asymmetries and that inclusion of this larger scheme in approaches like MFA might not be necessary, as the difference is likely going to be small \cite{wang2020metabolic}.
However, it is theoretically possible for the rate constants to be higher than the bounds we chose.
More experimental data is required to increase the confidence in the predictions made.

% Topic: potential targets for experimental work
In order to estimate the fluxes through the extended reactions we identified asymmetries and label patterns as useful targets.
One such target is the first carbon of \gls{g6p}, which with the addition of the extended reactions just requires three instead of seven enzymatic steps from \gls{dhap} to be labelled - all of which are extended reactions.
Thus, deviation from the expected labelling can be a useful hint for the total flux through the extended reactions.
Similarly, the third carbon of \gls{g6p} is predicted to be labelled less due to and only due to the extended reactions.
Here, again, the total flux can be estimated from a deviation from the expected label.

% Topic: hard to distinguish extensions vs transketolase increase
However, as the labelling pattern of carbons three and four of \gls{s7p} shows, those estimates require a good estimate of the \gls{tk} activity in general, as changes in \gls{tk} activity already cause changes in a multitude of labelling patterns.
Thus, when assessing the differences caused by the extended reactions it is important to distinguish them from changes caused by increased total \gls{tk} and \gls{ald} activity.
Further theoretical work is required to isolate these two effects.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Methods}\label{methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Topic: determination of the disequilibrium

$$k^+ = \frac{\frac{v}{\prod_i S_i}}{1 - \frac{Q}{K_{eq}}}$$

and respectively

$$k^- = - \frac{\frac{v}{\prod_j P_j}}{1 - \frac{K_{eq}}{Q}}$$


With the new obtained mass action rate parameters the corresponding reactions were brought into standard mass action format

\begin{equation}\label{eq:standard-mass-action}
	v = k^+ \prod_{i}S_i - k^- \prod_{j} P_j
\end{equation}

Similiarly, in order to fit the regularised reactions that follow the canonical expression

\begin{equation}
	v = \frac{V_{Max} \cdot [S]}{[S] + K_m \cdot R(S)}
\end{equation}

the $V_{Max}$ values were obtained using

\begin{equation}
	V_{Max} = \frac{v \left( [S] + K_m \cdot R(S) \right)}{[S]}
\end{equation}

and the experimentally measured concentrations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Funding}\label{funding}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This work was funded by the Deutsche Forschungsgemeinschaft (DFG) under Germany's Excellence Strategy EXC 2048/1, Project ID: 390686111 (O.E.) and EU's Horizon 2020 research and innovation programme under the Grant Agreement 862087 (M.v.A.).

\end{linenumbers}




% Notes
% % Non-standard
% In the non-standard reaction R5P + F6P \ce{<=>} E4P + S7P carbons 1 and 2 of F6P are transferred on top of R5P to form S7P, while the remaining carbons form E4P. The carbon transitions between R5P and S7P already occur in the standard \gls{tk} reactions, as do the transitions between F6P and E4P. The direct transition between F6P$^{12}$ and S7P$^{12}$ does not occur in the standard CBB scheme, but the transition between X5P$^{12}$ and S7P$^{12}$ and the transitions between F6P$^{12}$ and X5P$^{12}$ both exists. Thus, we do not expect this reaction to have a major influence on any labeling pattern.

% % Neutral
% The neutral reaction GAP + X5P \ce{<=>} GAP + X5P could further increase the labeling in the third carbon of X5P and the other pentoses, as well as potentially the fourth carbon of S7P. The reaction E4P + F6P \ce{<=>} E4P + F6P can increase the mixing between those compounds, but as the carbon transition is already present in the standard \gls{tk} reaction E4P + X5P \ce{<=>} F6P + GAP, the effect will be negligible. The same holds for last neutral reaction R5P + S7P \ce{<=>} R5P + S7P, which adds no new carbon transition routes to the standard \gls{tk} reaction R5P + X5P \ce{<=>} S7P + GAP.

% % Octose reactions
% In all possible octose reactions, O8P carbons 3-8 are always exchanged with G6P, thus they cannot change the labeling pattern. O8P carbons 1 and 2 however are exchanged with X5P, F6P and S7P, thus increasing the mixing velocity between those positions. However, since those positions are exchanged by the subsequent passing through both standard \gls{tk} reactions as well, we hypothesise that the effect will not be significant.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Topic: hypothetical addition of transaldolase
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transaldolase
% A reaction not present in the model is transaldolase (S7P + GAP \ce{<=>} E4P + F6P). In the reaction carbons 5 and 6 of F6P to be exchanged with carbons 2 and 3 of GAP, while carbons 1 and 2 of F6P are exchanged with carbons 1 and 2 of S7P. This could change the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries, depending on whether S7P$^{12}$ or GAP$^{23}$ is labeled more, which however is dependent on the pool sizes and relative enzyme activity inside the cycle and thus non-trivial to predict. Since GAP$^{1}$ is exchanged with F6P$^{4}$ and S7P$^{3}$ with F6P$^{3}$, the $\frac{C4}{C3}$ asymmetry might increase slightly, as GAP$^{1}$ will always be labeled more strongly than S7P$^{3}$. The exchange between carbons 4-7 of S7P and E4P is predicted to have a negligible effect, as those positions are labeled similarly.
