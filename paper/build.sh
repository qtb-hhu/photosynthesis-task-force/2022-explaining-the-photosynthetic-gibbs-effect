#!/bin/bash

NAME="main"

pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape main.tex || echo "failed, but continue running"
biber main  || echo "failed, but continue running"
pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape main.tex || echo "failed, but continue running"
pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape main.tex || echo "failed, but continue running"

